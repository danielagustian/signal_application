class CommentModel {
  int id, signalID, userID, commentID, like, dislike;
  String userName, comment;
  DateTime commentDate;
  List<CommentModel> child;

  CommentModel(
      {this.id,
      this.signalID,
      this.userID,
      this.userName,
      this.comment,
      this.commentDate,
      this.commentID,
      this.like,
      this.dislike,
      this.child});

  CommentModel.fromJson(Map<String, dynamic> map) {
    this.id = map["id"];
    this.signalID = map['signal_id'];
    this.userID = map['cms_user_id'];
    this.userName = map['userName'];
    this.comment = map['comment'];
    this.commentDate = DateTime.parse(map['created_at']);
    this.like = map['totalLike'];
    this.dislike = map['totalDislike'];

    // for child comment
    if (map["comment_id"] != null) this.commentID = map['comment_id'];
    if (map["child_comment"] != null)
      this.child = convertChildComment(map['child_comment']);
  }

  List<CommentModel> convertChildComment(List commentMap) {
    if (commentMap == null) {
      return null;
    }
    List<CommentModel> comments = [];
    commentMap.forEach((value) {
      comments.add(CommentModel.fromJson(value));
    });

    return comments;
  }
}
