import 'package:signal_app/models/user-model.dart';

class TrendingSignalModel {
  int id,
      readTime,
      userID,
      totalLove,
      totalHappy,
      totalSad,
      totalAngry,
      totalConfused;
  String judul,
      namaUser,
      slug,
      artikel,
      image,
      action,
      takeprofit,
      stoploss,
      pair;
  DateTime date;
  UserModel user;

  TrendingSignalModel(
      {this.id,
      this.userID,
      this.judul,
      this.slug,
      this.image,
      this.artikel,
      this.date,
      this.readTime,
      this.totalAngry,
      this.totalConfused,
      this.totalHappy,
      this.totalLove,
      this.totalSad,
      this.action,
      this.takeprofit,
      this.stoploss,
      this.pair,
      this.user});

  TrendingSignalModel.fromJson(Map<String, dynamic> map) {
    this.id = map["id"];
    this.userID = map["user_id"];
    this.slug = map["slug"];
    this.judul = map["judul"] != null ? map["judul"] : map["title"];
    this.readTime = map["readTime"];

    // it will work when it used for detail signal
    if (map["totalLove"] != null) this.totalLove = map["totalLove"];
    if (map["totalAngry"] != null) this.totalAngry = map["totalAngry"];
    if (map["totalSad"] != null) this.totalSad = map["totalSad"];
    if (map["totalConfused"] != null) this.totalConfused = map["totalConfused"];
    if (map["totalHappy"] != null) this.totalHappy = map["totalHappy"];
    if (map["artikel"] != null) this.artikel = map["artikel"];
    if (map["createdBy"] != null) this.namaUser = map["createdBy"];
    if (map["artikel"] != null) this.artikel = map["artikel"];
    if (map["image"] != null) this.image = map["image"];
    if (map["type"] != null) this.action = map["type"];
    if (map["stoploss"] != null) this.stoploss = map["stoploss"];
    if (map["takeprofit"] != null) this.takeprofit = map["takeprofit"];
    if (map["pair"] != null)
      this.pair = map["pair"] is String ? map["pair"] : map["pair"]["pair"];
    this.date = map["createdAt"] == null
        ? DateTime.parse(map["created_at"])
        : DateTime.parse(map["createdAt"]);
    if (map["user"] != null) this.user = UserModel.fromJson(map["user"]);
  }
}
