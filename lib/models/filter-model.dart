class FilterModel {
  String pair, action, actValue;

  FilterModel({this.pair, this.action, this.actValue});

  FilterModel.fromJson(Map<String, dynamic> map) {
    this.pair = map["pair"];
    this.actValue = map["action"];

    // SET VALUE TO SHOW
    String act = "SELL";
    if (map["action"] == "OP_BUY") act = "BUY";
    else if (map["action"] == "OP_SELLLIMIT") act = "SELL LIMIT";
    this.action = act;
  }
}
