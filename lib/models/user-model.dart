class UserModel {
  String fullname, email, password, username, imgUrl, bio;
  int userID, followerCount, followingCount, postingCount;

  UserModel({
    this.userID,
    this.fullname,
    this.username,
    this.email,
    this.password,
    this.bio,
    this.followerCount,
    this.followingCount,
    this.postingCount,
    this.imgUrl,
  });

  Map<String, dynamic> toJson(UserModel user) {
    var map = Map<String, dynamic>();
    map["email"] = user.email;
    map["password"] = user.password;
    map["username"] = user.username;
    map["fullname"] = user.fullname;
    if (user.bio == null) map["deskripsi"] = user.bio;
    if (user.followerCount == null) map["totalFollowers"] = user.followerCount;
    if (user.followingCount == null)
      map["totalFollowing"] = user.followingCount;
    if (user.postingCount == null) map["totalSignal"] = user.postingCount;
    if (user.imgUrl == null) map["photo"] = user.imgUrl;

    return map;
  }

  UserModel.fromJson(Map<String, dynamic> map) {
    this.userID = map["id"];
    this.email = map["email"];
    this.fullname = map["name"];
    this.username = map["username"];
    this.followerCount =
        map["followerCount"] == null ? 0 : map["followerCount"];
    this.followingCount =
        map["followingCount"] == null ? 0 : map["followingCount"];
    this.postingCount = map["postingCount"] == null ? 0 : map["postingCount"];
    this.bio = map["bio"] == null ? "" : map["bio"];
    this.imgUrl = map["photo"] == null ? "" : map["imgUrl"];
  }
}
