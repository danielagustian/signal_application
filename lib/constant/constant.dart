import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const colorPrimary = Color(0xFF1C7335);
const colorPrimary70 = Color(0xFF609D72);
const colorPrimary50 = Color(0xFF8DB99A);
const colorPrimary30 = Color(0xFFBBD5C2);
const colorSecondaryRed = Color(0xFFF05769);
const colorSecondaryRed70 = Color(0xFFF58A96);
const colorSecondaryRed50 = Color(0xFFF8ABB4);
const colorSecondaryRed30 = Color(0xFFFBCDD2);
const colorSecondaryYellow = Color(0xFFFFD602);
const colorSecondaryYellow70 = Color(0xFFFFE34E);
const colorSecondaryYellow50 = Color(0xFFFFEB81);
const colorSecondaryYellow30 = Color(0xFFFFF3B4);

const colorClear = Color(0xFF1BB55C);
const colorError = Color(0xFFE74C3C);

const colorSemanticYellow = Color(0xFFFFCF5C);
const colorSemanticBlue = Color(0xFF0084F4);
const colorSemanticRed = Color(0xFFFF647C);
const colorSemanticBlack = Color(0xFF1C1C1C);
const colorSemanticGreen = Color(0xFF00C48C);

const colorNeutral = Color(0xFFE1DEE5);
const colorNeutral70 = Color(0xFFEAE8ED);
const colorNeutral50 = Color(0xFFF0EFF2);
const colorNeutral30 = Color(0xFFF6F6F8);
const colorDarkNeutral = Color(0xFFC4C4C4);

const colorBackground = Color(0xFFF9FBFB);
const colorGoogle = Color(0xFF4081EC);
