import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:signal_app/constant/constant.dart';

class HeaderWithSearchBar extends StatelessWidget {
  const HeaderWithSearchBar({
    Key key,
    @required this.size,
    @required this.textSearch,
  }) : super(key: key);

  final Size size;
  final TextEditingController textSearch;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.only(bottom: 30),
          height: size.height * 0.2,
          child: Container(
            decoration: BoxDecoration(
                color: colorPrimary,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(36),
                  bottomRight: Radius.circular(36),
                )),
            child: Container(
              width: size.width,
              margin: EdgeInsets.only(left: 20, top: size.height * 0.055),
              child: Text(
                "Trending Ideas",
                style: TextStyle(
                    color: colorBackground,
                    fontSize: size.height < 600 ? 22 : 25,
                    fontWeight: FontWeight.w700),
              ),
            ),
          ),
        ),
        // Make a Search Bar
        Positioned(
          // Make the component flow in the center-end of Header
          bottom: 10,
          left: 0,
          right: 0,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            alignment:
                Alignment.center, // Make the hint in the middle vertically
            height: 50,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: colorBackground,
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 10),
                      blurRadius: 50,
                      color: colorPrimary.withOpacity(0.3))
                ]),
            child: TextFormField(
              cursorColor: colorPrimary.withOpacity((0.5)),
              textInputAction: TextInputAction.search,
              keyboardType: TextInputType.text,
              onChanged: (val) {},
              controller: textSearch,
              decoration: InputDecoration(
                  focusColor: colorPrimary.withOpacity(0.7),
                  contentPadding: EdgeInsets.symmetric(horizontal: 20),
                  hintText: "Search",
                  hintStyle: TextStyle(color: colorPrimary.withOpacity(0.7)),
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none),
            ),
          ),
        )
      ],
    );
  }
}
