import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:signal_app/constant/constant.dart';

class CommentItem extends StatelessWidget {
  const CommentItem({
    Key key,
    @required this.size,
    this.child: false,
    this.username,
    this.time,
    this.comment,
    this.voteUp: "0",
    this.voteDown: "0",
    this.clickReply,
    this.callbackVU,
    this.callbackVD,
  }) : super(key: key);

  final Size size;
  final bool child;
  final String username, time, comment, voteUp, voteDown;
  final Function clickReply, callbackVU, callbackVD;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(horizontal: 20),
      width: size.width,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CircleAvatar(
            backgroundColor: colorGoogle,
            radius: child ? 15 : 20,
          ),
          SizedBox(width: 15),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("$username",
                          style: TextStyle(
                              fontSize: size.height < 600 ? 14 : 16,
                              fontWeight: FontWeight.w700)),
                      Text("$time",
                          style: TextStyle(
                              color: colorNeutral,
                              fontSize: size.height < 600 ? 13 : 15)),
                    ],
                  )),
              SizedBox(height: 5),
              Text("$comment",
                  style: TextStyle(fontSize: size.height < 600 ? 13 : 15)),
              Container(
                  width: size.width,
                  child: Row(
                    children: [
                      InkWell(
                        onTap: callbackVU,
                        child: FaIcon(
                          FontAwesomeIcons.caretUp,
                          color: colorPrimary,
                          size: 36,
                        ),
                      ),
                      SizedBox(width: 5),
                      Text("$voteUp"),
                      SizedBox(width: 12),
                      InkWell(
                        onTap: callbackVD,
                        child: FaIcon(
                          FontAwesomeIcons.caretDown,
                          color: colorSemanticRed,
                          size: 36,
                        ),
                      ),
                      SizedBox(width: 5),
                      Text("$voteDown"),
                      SizedBox(width: 16),
                      InkWell(
                        onTap: clickReply,
                        child: Text("Reply",
                            style: TextStyle(
                                color: colorPrimary,
                                fontSize: size.height < 600 ? 13 : 15)),
                      ),
                    ],
                  )),
            ],
          ))
        ],
      ),
    );
  }
}
