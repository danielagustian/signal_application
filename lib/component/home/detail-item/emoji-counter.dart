import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:signal_app/constant/constant.dart';

class EmojiCounter extends StatelessWidget {
  const EmojiCounter({
    Key key,
    this.emoji,
    this.counter,
    this.size,
  }) : super(key: key);

  final String emoji, counter;
  final Size size;

  @override
  Widget build(BuildContext context) {
    
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: size.width < 400 ? 6 : 8, vertical: 6),
      decoration: BoxDecoration(
          color: colorPrimary30, borderRadius: BorderRadius.circular(5)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset("$emoji"),
          SizedBox(width: 5),
          Text(
            "$counter ",
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: Colors.black26, fontSize: size.width < 400 ? 12 : 14),
          )
        ],
      ),
    );
  }
}
