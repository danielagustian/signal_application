import 'dart:async';

import 'package:flutter/material.dart';
import 'package:signal_app/constant/constant.dart';

// ignore: must_be_immutable
class CustomCarousel extends StatelessWidget {
  CustomCarousel({
    Key key,
    @required this.size,
    this.banners,
  }) : super(key: key);

  final Size size;
  final List banners;
  PageController _controller = PageController(initialPage: 0);
  int pageIndex = 0;

  @override
  Widget build(BuildContext context) {
    if (banners != null) {
      Timer.periodic(Duration(seconds: 5), (Timer timer) {
        if (pageIndex < banners.length) {
          pageIndex++;
        } else {
          pageIndex = 0;
        }

        if (_controller.hasClients)
          _controller.animateToPage(
            pageIndex,
            duration: Duration(milliseconds: 400),
            curve: Curves.easeIn,
          );
      });
    }

    return Container(
      width: size.width,
      height: 200,
      child: PageView.builder(
        onPageChanged: (value) => pageIndex = value,
        controller: _controller,
        reverse: false,
        itemCount: banners == null ? 0 : banners.length,
        itemBuilder: (context, index) => banners == null
            ? nullCarouselItem()
            : carouselItem(imgURL: banners[index]),
      ),
    );
  }

  // Build carousel item
  Widget carouselItem({String imgURL}) {
    return Stack(
      children: [
        Positioned(
          top: 12,
          bottom: 12,
          right: 12,
          left: 12,
          child: Container(
              width: size.width,
              height: 180,
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                    offset: Offset(0, 0),
                    color: colorSemanticBlack.withOpacity(0.5),
                    blurRadius: 5)
              ]),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.network(
                  "$imgURL",
                  fit: BoxFit.cover,
                ),
              )),
        ),
        Positioned(
          bottom: 12,
          right: 12,
          left: 12,
          child: Container(
            padding: EdgeInsets.fromLTRB(8, 4, 8, 8),
            decoration: BoxDecoration(
                color: colorSemanticBlack.withOpacity(0.2),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "NEUTRAL - BTCUSD",
                  style: TextStyle(
                      color: colorBackground,
                      fontWeight: FontWeight.w600,
                      fontSize: size.height <= 600 ? 10 : 12),
                ),
                Text(
                  "Bitconins reaches 50,000 USD after blba bla bla asfs  saf s sadf a  sf asf a as f",
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                      color: colorBackground,
                      fontWeight: FontWeight.w600,
                      fontSize: size.height <= 600 ? 10 : 12),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  // Build null carousel item
  Widget nullCarouselItem({String imgURL}) {
    return Stack(
      children: [
        Positioned(
          top: 12,
          bottom: 12,
          right: 12,
          left: 12,
          child: Container(
            width: size.width,
            height: 180,
            decoration: BoxDecoration(color: colorDarkNeutral, boxShadow: [
              BoxShadow(
                  offset: Offset(0, 0),
                  color: colorSemanticBlack.withOpacity(0.5),
                  blurRadius: 15)
            ]),
          ),
        ),
      ],
    );
  }
}
