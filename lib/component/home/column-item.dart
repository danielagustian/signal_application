import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColumnItem extends StatelessWidget {
  const ColumnItem({
    Key key,
    this.title,
    this.subtitle,
    this.width,
  }) : super(key: key);

  final String title, subtitle;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "$title",
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w700, fontSize: 16),
          ),
          Text(
            "$subtitle",
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w700, fontSize: 22),
          ),
        ],
      ),
    );
  }
}
