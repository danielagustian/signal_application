import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LongButtonIcon extends StatelessWidget {
  const LongButtonIcon({
    Key key,
    @required this.size,
    @required this.title,
    this.onClick,
    this.bgColor: Colors.white,
    this.textColor: Colors.black,
    this.iconWidget,
  }) : super(key: key);

  final Size size;
  final Function onClick;
  final String title;
  final Color bgColor, textColor;
  final Widget iconWidget;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onClick,
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(8.0),
        primary: bgColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
      ),
      child: Container(
        width: size.width,
        height: size.height * 0.045,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          iconWidget,
          SizedBox(width: 15),
          Center(
            child: Text(title),
          ),
        ]),
      ),
    );
  }
}
