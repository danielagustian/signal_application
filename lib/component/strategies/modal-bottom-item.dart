import 'package:flutter/material.dart';
import 'package:signal_app/constant/constant.dart';

class ModalBottomSheetItem extends StatelessWidget {
  const ModalBottomSheetItem({
    Key key,
    this.text1,
    this.text2,
  }) : super(key: key);

  final String text1, text2;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("$text1",
              style: TextStyle(color: colorSemanticBlack, fontSize: 12)),
          Text("$text2",
              style: TextStyle(
                  color: colorSemanticBlack,
                  fontWeight: FontWeight.bold,
                  fontSize: 12)),
        ],
      ),
    );
  }
}
