import 'package:flutter/material.dart';
import 'package:signal_app/constant/constant.dart';

class StrategiesDetailItem extends StatelessWidget {
  const StrategiesDetailItem({
    Key key,
    this.title,
    this.time,
    this.tp,
    this.sl,
    this.size,
  }) : super(key: key);

  final String title, time, tp, sl;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                blurRadius: 20,
                offset: Offset(0, 4),
                color: Color(0xFF505050).withOpacity(0.1))
          ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "$title ",
            style: TextStyle(
                color: colorPrimary,
                fontSize: size.height <= 600 ? 18 : 20,
                fontWeight: FontWeight.bold),
          ),
          Container(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "$time",
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: colorDarkNeutral,
                  fontSize: 12,
                ),
              ),
              Text(
                "T/P: $tp",
                style: TextStyle(
                  color: colorDarkNeutral,
                  fontSize: 12,
                ),
              ),
              Text(
                "S/L: $sl",
                style: TextStyle(
                  color: colorDarkNeutral,
                  fontSize: 12,
                ),
              )
            ],
          ))
        ],
      ),
    );
  }
}
