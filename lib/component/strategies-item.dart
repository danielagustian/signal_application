import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:signal_app/constant/constant.dart';

class ColumnTextBlacPrimary extends StatelessWidget {
  const ColumnTextBlacPrimary({
    Key key,
    this.text1,
    this.text2,
    this.size,
  }) : super(key: key);

  final String text1, text2;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "$text1",
            textAlign: TextAlign.justify,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontSize: size.height < 600 ? 11 : 13),
          ),
          Text(
            "$text2",
            textAlign: TextAlign.justify,
            style: TextStyle(
                color: colorPrimary,
                fontWeight: FontWeight.bold,
                fontSize: size.height < 600 ? 18 : 22),
          ),
        ],
      ),
    );
  }
}
