import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LongButton extends StatelessWidget {
  const LongButton({
    Key key,
    @required this.size,
    @required this.title,
    this.onClick,
    this.bgColor: Colors.white,
    this.textColor: Colors.black,
    this.loading: false,
  }) : super(key: key);

  final Size size;
  final Function onClick;
  final String title;
  final Color bgColor, textColor;
  final bool loading;

  @override
  Widget build(BuildContext context) {
    print(size.height);
    return ElevatedButton(
      onPressed: onClick,
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(8.0),
        primary: bgColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      child: AnimatedContainer(
        width: loading ? 80 : size.width,
        height: size.height >= 800 ? size.height * 0.04 : size.height * 0.05,
        duration: Duration(milliseconds: 600),
        curve: Curves.fastOutSlowIn,
        child: Center(
            child: loading
                ? CircularProgressIndicator(
                    backgroundColor: bgColor,
                    valueColor: AlwaysStoppedAnimation(textColor),
                  )
                : Text(
                    title,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: textColor),
                  )),
      ),
    );
  }
}
