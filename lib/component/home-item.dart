import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:signal_app/constant/constant.dart';

class CardItem extends StatelessWidget {
  const CardItem({
    Key key,
    this.title,
    this.badge,
    this.user,
    this.date,
    this.reads: "0 mins",
    @required this.size,
  }) : super(key: key);

  final String title, badge, user, date, reads;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
      decoration: BoxDecoration(
          color: colorBackground,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 4),
                blurRadius: 20,
                color: Color.fromRGBO(80, 80, 80, 0.1))
          ]),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width:
                      size.height < 600 ? size.width * 0.5 : size.width * 0.55,
                  child: Text(
                    "$title",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                        fontSize: 15),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                  decoration: BoxDecoration(
                      color: badge.contains("SELL")
                          ? colorSecondaryRed
                          : colorPrimary,
                      borderRadius: BorderRadius.circular(6)),
                  child: Text(
                    "$badge",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: colorBackground,
                        fontWeight: FontWeight.w700,
                        fontSize: 11),
                  ),
                )
              ],
            ),
          ),
          Divider(
            height: 10,
            thickness: 1,
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  "$user",
                  style: TextStyle(
                      color: colorNeutral,
                      fontWeight: FontWeight.w700,
                      fontSize: 12),
                ),
                Text(
                  "$date ($reads)",
                  style: TextStyle(
                      color: colorNeutral,
                      fontWeight: FontWeight.w500,
                      fontSize: 12),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
