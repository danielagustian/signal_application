import 'package:flutter/material.dart';
import 'package:signal_app/constant/constant.dart';

class SettingItem extends StatelessWidget {
  const SettingItem({
    Key key,
    @required this.size,
    this.widget,
    this.onClick,
    this.title,
  }) : super(key: key);

  final Size size;
  final Widget widget;
  final Function onClick;
  final String title;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            widget,
            SizedBox(width: 18),
            Text("$title",
                style: TextStyle(
                    color: colorSemanticBlack,
                    fontSize: size.height <= 600 ? 14 : 16))
          ],
        ),
      ),
    );
  }
}