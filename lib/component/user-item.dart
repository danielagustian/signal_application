import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:signal_app/constant/constant.dart';

class UserItem extends StatelessWidget {
  const UserItem({
    Key key,
    this.fullname,
    this.username,
    this.imgURL,
    this.size,
  }) : super(key: key);

  final String fullname, username, imgURL;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size.width,
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
      color: colorBackground,
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: Container(
              height: 45,
              width: 45,
              color: colorDarkNeutral,
              child: imgURL == null || imgURL == ""
                  ? Container()
                  : Image.network(
                      "$imgURL",
                      fit: BoxFit.cover,
                    ),
            ),
          ),
          SizedBox(width: 10),
          Text(
            "$fullname",
            style: TextStyle(
                color: colorSemanticBlack,
                fontSize: 16,
                fontWeight: FontWeight.w700),
          ),
          SizedBox(width: 10),
          CircleAvatar(
            radius: 3,
            backgroundColor: colorDarkNeutral,
          ),
          SizedBox(width: 10),
          Text(
            "$username",
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: colorDarkNeutral,
              fontSize: 12,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }
}