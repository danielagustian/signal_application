import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:signal_app/bloc/trending/trending-bloc.dart';
import 'package:signal_app/bloc/trending/trending-event.dart';
import 'package:signal_app/bloc/trending/trending-state.dart';
import 'package:signal_app/component/home-item.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/models/trending-signal-model.dart';
import 'package:signal_app/screen/home/detail-item-screen.dart';
import 'package:signal_app/screen/search-screen.dart';
import 'package:signal_app/utils/util.dart';

class FilteredSignalScreen extends StatefulWidget {
  const FilteredSignalScreen({
    Key key,
    this.filter,
    this.filterName,
  }) : super(key: key);

  @override
  _FilteredSignalScreenState createState() => _FilteredSignalScreenState();
  final String filterName, filter;
}

class _FilteredSignalScreenState extends State<FilteredSignalScreen> {
  TextEditingController textSearch = new TextEditingController();
  List<String> values = ["Trending", "Latest"];
  String choose = "Trending";

  DateFormat formatter = DateFormat.yMMMMd();
  List<TrendingSignalModel> trendings;
  bool loading = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<TrendingBloc>(context).add(TrendingEventLoadFilteredData(
        queryParams: widget.filter.toLowerCase()));
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: colorBackground,
      appBar: AppBar(
        backgroundColor: colorBackground,
        elevation: 0,
        leading: IconButton(
          icon: FaIcon(
            FontAwesomeIcons.chevronLeft,
            color: colorPrimary,
          ),
          onPressed: () {
            BlocProvider.of<TrendingBloc>(context).add(TrendingEventLoadData());
            Navigator.pop(context);
          },
        ),
        title: Text(
          "${widget.filterName.toUpperCase()}",
          style: TextStyle(
              color: Colors.black,
              fontSize: size.height < 600 ? 22 : 25,
              fontWeight: FontWeight.w700),
        ),
        actions: [
          IconButton(
              icon: FaIcon(
                FontAwesomeIcons.search,
                color: colorPrimary,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchScreen()));
              })
        ],
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            alignment: Alignment.centerLeft,
            child: DropdownButton(
              value: choose,
              icon: FaIcon(
                FontAwesomeIcons.chevronDown,
                color: colorPrimary,
              ),
              style: TextStyle(
                  color: colorPrimary,
                  fontWeight: FontWeight.w700,
                  fontSize: 16),
              underline: Container(),
              onChanged: (value) {
                onSelectedItem(value);
              },
              items: values.map<DropdownMenuItem<String>>((String val) {
                return DropdownMenuItem<String>(
                  value: val,
                  child: Text(
                    val,
                    style: TextStyle(
                        fontSize: 16,
                        color: colorPrimary,
                        fontWeight: FontWeight.w700),
                  ),
                );
              }).toList(),
            ),
          ),
          BlocListener<TrendingBloc, TrendingState>(
              listener: (context, state) {
                if (state is TrendingStateLoading) {
                  setState(() {
                    loading = true;
                  });
                } else if (state is TrendingStateSuccessLoadFilteredSignal) {
                  setState(() {
                    trendings = state.trendings;
                    loading = false;
                  });
                } else {
                  setState(() {
                    loading = false;
                  });
                  Util().showToast(
                    context: this.context,
                    msg: "Something Wrong !",
                    color: colorError,
                    txtColor: colorBackground,
                  );
                }
              },
              child: trendings == null
                  ? Center(
                      child: CircularProgressIndicator(
                        backgroundColor: colorBackground,
                        valueColor: AlwaysStoppedAnimation(colorPrimary),
                      ),
                    )
                  : trendings == null
                      ? Container()
                      : Expanded(
                          child: Container(
                            margin: size.width < 500
                                ? EdgeInsets.zero
                                : EdgeInsets.symmetric(horizontal: 30),
                            child: ListView.builder(
                              padding: EdgeInsets.zero,
                              itemCount: trendings.length,
                              itemBuilder: (context, index) {
                                String action = "SELL";
                                if (trendings[index].action == "OP_BUY")
                                  action = "BUY";

                                return InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => DetailItem(
                                                id: trendings[index].id)));
                                  },
                                  child: CardItem(
                                    size: size,
                                    title: "${trendings[index].judul}",
                                    badge:
                                        "${trendings[index].pair.toUpperCase()} $action",
                                    date:
                                        "${formatter.format(trendings[index].date)}",
                                    user: "${trendings[index].namaUser}",
                                    reads: "${trendings[index].readTime} mins",
                                  ),
                                );
                              },
                            ),
                          ),
                        )),
          // Expanded(
          //   child: Container(
          //     margin: size.width < 500
          //         ? EdgeInsets.zero
          //         : EdgeInsets.symmetric(horizontal: 30),
          //     child: BlocBuilder<TrendingBloc, TrendingState>(
          //       builder: (context, state) {
          //         if (state is TrendingStateSuccessLoad) {
          //           return ListView.builder(
          //             padding: EdgeInsets.zero,
          //             itemCount: state.trendings.length,
          //             itemBuilder: (context, index) {
          //               String action = "SELL";
          //               if (state.trendings[index].action == "OP_BUY")
          //                 action = "BUY";

          //               return InkWell(
          //                 onTap: () {
          //                   Navigator.push(
          //                       context,
          //                       MaterialPageRoute(
          //                           builder: (context) => DetailItem(
          //                               id: state.trendings[index].id)));
          //                 },
          //                 child: CardItem(
          //                   size: size,
          //                   title: "${state.trendings[index].judul}",
          //                   badge:
          //                       "${state.trendings[index].pair.toUpperCase()} $action",
          //                   date:
          //                       "${formatter.format(state.trendings[index].date)}",
          //                   user: "${state.trendings[index].namaUser}",
          //                   reads: "${state.trendings[index].readTime} mins",
          //                 ),
          //               );
          //             },
          //           );
          //         }
          //         return Container();
          //       },
          //     ),
          //   ),
          // ),
          SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }

  void onSelectedItem(String val) {
    setState(() {
      choose = val;
    });
  }
}
