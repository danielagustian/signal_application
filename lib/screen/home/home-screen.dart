import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:signal_app/bloc/trending/trending-bloc.dart';
import 'package:signal_app/bloc/trending/trending-event.dart';
import 'package:signal_app/bloc/trending/trending-state.dart';
import 'package:signal_app/component/home-item.dart';
import 'package:signal_app/component/home/custom-carousel.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/screen/home/detail-item-screen.dart';
import 'package:signal_app/screen/home/filtered-signal-screen.dart';
import 'package:signal_app/screen/search-screen.dart';
import 'package:signal_app/services/filter-service.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  TextEditingController textSearch = new TextEditingController();

  // Dropdown Button
  List<String> values = ["Trending", "Latest"];
  String choose = "Trending";

  // Draggable scroll controller
  AnimationController _controller;
  Duration _duration = Duration(milliseconds: 600);
  Tween<Offset> _tween = Tween(begin: Offset(0, 1), end: Offset(0, 0));

  // Category list
  List<String> categories = [
    "Latest",
    "Following",
    "Credibility",
    "Popularity"
  ];

  // For Filter
  String choosedFilter;

  // For DragabbleSheet Filter
  String choosedPairFilter = "A921MD0";

  // date formatter
  DateFormat formatter = DateFormat.yMMMMd();
  DateFormat highlightFormatter = DateFormat.MMMMd();
  DateTime now = DateTime.now();

  // list image dummy
  List banner = [
    "https://cdn-2.tstatic.net/tribunnews/foto/bank/images/forex-260819-3.jpg",
    "https://duwitmu.com/wp-content/uploads/2019/12/Forex-Trading-Trader.jpg.webp",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSDXK7qzVTbGP1rCf6agFSv9JCmViTjtJOYhg&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQf-5EwWRCOJjc-N7KTxUk5a6GRzuDI1s8CgQ&usqp=CAU"
  ];

  // For filter list
  FilterService _filterService = FilterService();
  List actionList = [];
  List pairList = [];

  // function to get all the filter
  void getFilterList() async {
    var actResult = await _filterService.getFilterCategory(category: "action");
    var pairResult = await _filterService.getFilterCategory(category: "pairs");

    setState(() {
      actionList = actResult;
      pairList = pairResult;
    });
  }

  @override
  void initState() {
    super.initState();
    choosedFilter = categories[0];
    BlocProvider.of<TrendingBloc>(context).add(TrendingEventLoadData());

    // Init for dragabble controller
    _controller = AnimationController(vsync: this, duration: _duration);

    // get all filter data
    getFilterList();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: colorBackground,
      appBar: AppBar(
        backgroundColor: colorBackground,
        elevation: 0,
        automaticallyImplyLeading: false,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Trending Ideas",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: size.height < 600 ? 16 : 18,
                  fontWeight: FontWeight.w700),
            ),
            SizedBox(height: 5),
            Text(
              "${highlightFormatter.format(now)}",
              style: TextStyle(
                  color: colorSemanticBlack.withOpacity(0.5),
                  fontSize: size.height < 600 ? 14 : 16,
                  fontWeight: FontWeight.w700),
            ),
          ],
        ),
        actions: [
          IconButton(
              icon: FaIcon(
                FontAwesomeIcons.search,
                color: colorPrimary,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchScreen()));
              })
        ],
      ),
      body: RefreshIndicator(
        color: colorPrimary,
        onRefresh: () async {
          BlocProvider.of<TrendingBloc>(context).add(TrendingEventLoadData());
        },
        child: Stack(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: [
                  CustomCarousel(size: size, banners: banner),
                  SizedBox(height: 25),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Category",
                          style: TextStyle(
                              height: 0.3,
                              color: Colors.black,
                              fontSize: size.height < 600 ? 16 : 18,
                              fontWeight: FontWeight.w700),
                        ),
                        InkWell(
                          onTap: () {
                            if (_controller.isDismissed) {
                              _controller.forward();
                            } else if (_controller.isCompleted) {
                              _controller.reverse();
                            }
                          },
                          child: Text("See all",
                              style: TextStyle(
                                color: colorPrimary,
                                fontSize: size.height < 600 ? 10 : 12,
                              )),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 15),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: 70,
                          height: 90,
                          decoration: BoxDecoration(
                              color: colorBackground,
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                    offset: Offset(0, 4),
                                    blurRadius: 20,
                                    color: colorSemanticBlack.withOpacity(0.1))
                              ]),
                        ),
                        Container(
                          width: 70,
                          height: 90,
                          decoration: BoxDecoration(
                              color: colorBackground,
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                    offset: Offset(0, 4),
                                    blurRadius: 20,
                                    color: colorSemanticBlack.withOpacity(0.1))
                              ]),
                        ),
                        Container(
                          width: 70,
                          height: 90,
                          decoration: BoxDecoration(
                              color: colorBackground,
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                    offset: Offset(0, 4),
                                    blurRadius: 20,
                                    color: colorSemanticBlack.withOpacity(0.1))
                              ]),
                        ),
                        Container(
                          width: 70,
                          height: 90,
                          decoration: BoxDecoration(
                              color: colorBackground,
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                    offset: Offset(0, 4),
                                    blurRadius: 20,
                                    color: colorSemanticBlack.withOpacity(0.1))
                              ]),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 25),
                  Container(
                    width: size.width,
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          ...categories.map(
                            (val) => InkWell(
                                onTap: () {
                                  onChangeFilter(val);
                                },
                                child: filterItem(title: val)),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    margin: size.width < 500
                        ? EdgeInsets.zero
                        : EdgeInsets.symmetric(horizontal: 30),
                    child: BlocBuilder<TrendingBloc, TrendingState>(
                      builder: (context, state) {
                        if (state is TrendingStateSuccessLoad) {
                          return Column(
                            children: [
                              ...state.trendings.map((data) {
                                String action = "SELL";
                                if (data.action == "OP_BUY") action = "BUY";

                                return InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailItem(id: data.id)));
                                  },
                                  child: CardItem(
                                    size: size,
                                    title: "${data.judul}",
                                    badge: "${data.pair.toUpperCase()} $action",
                                    date: "${formatter.format(data.date)}",
                                    user: "${data.namaUser}",
                                    reads: "${data.readTime} mins",
                                  ),
                                );
                              })
                            ],
                          );
                        }
                        return Container();
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
            scrollBottomSheet(size),
          ],
        ),
      ),
    );
  }

  void onSelectedItem(String val) {
    setState(() {
      choose = val;
    });
  }

  // change filter handler
  void onChangeFilter(String val) {
    setState(() {
      choosedFilter = val;
    });
  }

  // Show scrollable sheet
  Widget scrollBottomSheet(Size size) {
    return Container(
      child: SlideTransition(
        position: _tween.animate(_controller),
        child: DraggableScrollableSheet(
          initialChildSize: 0.5,
          maxChildSize: 0.9,
          minChildSize: 0.2,
          builder: (context, scrollController) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              decoration: BoxDecoration(
                  color: colorBackground,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 20,
                        offset: Offset(0, -4),
                        color: Color(0xFF505050).withOpacity(0.5))
                  ]),
              child: ListView(
                controller: scrollController,
                children: [
                  Align(
                    child: Container(
                        height: 5,
                        width: 50,
                        decoration: BoxDecoration(
                          color: colorDarkNeutral,
                          borderRadius: BorderRadius.circular(20),
                        )),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: InkWell(
                      onTap: () => _controller.reverse(),
                      child: Text(
                        "Cancel",
                        style: TextStyle(
                          color: colorSemanticRed,
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  buildFilterBody(size, title: "Pair"),
                  SizedBox(height: 20),
                  buildFilterBody(size, title: "Action"),
                  SizedBox(height: 10),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  // Function to build filter body
  Widget buildFilterBody(Size size, {String title}) {
    List filterList = [];
    if (title == "Pair") {
      filterList = pairList;
    } else {
      filterList = actionList;
    }
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          filterTitle(size, title: "$title"),
          SizedBox(height: 14),
          Container(
              width: size.width,
              child: filterList.length == 0
                  ? Container()
                  : GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          mainAxisSpacing: 5,
                          crossAxisCount: 4,
                          crossAxisSpacing: 10,
                          childAspectRatio: 5.5 / 2),
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: filterList.length,
                      itemBuilder: (context, index) => InkWell(
                          onTap: () {
                            String filter = filterList[index];
                            String filterValue = "";

                            // data that send to filtered screen
                            if (title == "Action") {
                              if (filterList[index] == "OP_BUY") {
                                filterValue = "BUY";
                              } else if (filterList[index] == "OP_SELL") {
                                filterValue = "SELL";
                              } else {
                                filterValue = "SELL LIMIT";
                              }
                            } else {
                              filterValue =
                                  filterList[index].toString().toUpperCase();
                            }

                            // close the draggable sheet
                            _controller.reverse();

                            // move to filtered screen
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FilteredSignalScreen(
                                          filter: filter,
                                          filterName: filterValue,
                                        )));
                          },
                          child: filterItem(title: "${filterList[index]}")),
                    ))
        ],
      ),
    );
  }

  // Filter item title
  Widget filterTitle(Size size, {String title}) {
    return Text(
      "$title",
      style: TextStyle(
          color: colorSemanticBlack,
          fontWeight: FontWeight.w600,
          fontSize: size.height <= 600 ? 16 : 18),
    );
  }

  // Filter title
  Widget filterItem({String title}) {
    return Container(
      margin: EdgeInsets.only(right: 5),
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 8),
      decoration: BoxDecoration(
          color: title == choosedFilter ? colorPrimary : colorBackground,
          border: Border.all(
              color: title == choosedFilter ? colorPrimary : colorDarkNeutral,
              width: 1),
          borderRadius: BorderRadius.circular(6)),
      child: Center(
        child: Text(
          "$title",
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color:
                  title == choosedFilter ? colorBackground : colorDarkNeutral,
              fontWeight: FontWeight.w500,
              fontSize: 12),
        ),
      ),
    );
  }
}
