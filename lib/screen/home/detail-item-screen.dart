import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:signal_app/bloc/comment/comment-bloc.dart';
import 'package:signal_app/bloc/comment/comment-event.dart';
import 'package:signal_app/bloc/comment/comment-state.dart';
import 'package:signal_app/bloc/trending/trending-detail/trending-detail-bloc.dart';
import 'package:signal_app/bloc/trending/trending-detail/trending-detail-event.dart';
import 'package:signal_app/bloc/trending/trending-detail/trending-detail-state.dart';
import 'package:signal_app/component/home/column-item.dart';
import 'package:signal_app/component/home/detail-item/comment-item.dart';
import 'package:signal_app/component/home/detail-item/emoji-counter.dart';
import 'package:signal_app/component/long-button.dart';
import 'package:signal_app/component/strategies/modal-bottom-item.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/models/comment-model.dart';
import 'package:signal_app/models/trending-signal-model.dart';
import 'package:signal_app/utils/util.dart';

class DetailItem extends StatefulWidget {
  const DetailItem({Key key, this.id}) : super(key: key);

  @override
  _DetailItemState createState() => _DetailItemState();
  final int id;
}

class _DetailItemState extends State<DetailItem> with TickerProviderStateMixin {
  TextEditingController textComment = new TextEditingController();
  final scaffoldState = GlobalKey<ScaffoldState>();

  // Draggable scroll controller
  AnimationController _controller;
  Duration _duration = Duration(milliseconds: 600);
  Tween<Offset> _tween = Tween(begin: Offset(0, 1), end: Offset(0, 0));

  // Date formater
  DateFormat formatter = DateFormat.yMMMMd();

  // handle focus text on click "reply"
  FocusNode focusReply = FocusNode();

  // Store detail signal data
  TrendingSignalModel trend;
  String action = "SELL";

  // Store comment data
  List<CommentModel> comentList = [];

  // handle reply comment
  int parrentCommentID;
  String parrentUsername;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<TrendingDetailBloc>(context)
        .add(TrendingDetailEventLoadSignal(signalID: widget.id));

    // Init for dragabble controller
    _controller = AnimationController(vsync: this, duration: _duration);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return BlocListener<TrendingDetailBloc, TrendingDetailState>(
      listener: (context, state) {
        if (state is TrendingDetailStateLoading) {
        } else if (state is TrendingDetailStateSuccessLoad) {
          // Handle detail signal data
          BlocProvider.of<CommentBloc>(context)
              .add(CommentEventLoadCommentSignal(signalID: widget.id));

          setState(() {
            trend = state.signal;
            if (trend.action == "OP_BUY") action = "BUY";
          });
          print("state");
          print(trend.id);
        }
      },
      child: trend == null
          ? Scaffold(
              backgroundColor: colorBackground,
              body: Container(
                child: Center(
                    // child: Text("No data found"),
                    ),
              ),
            )
          : Scaffold(
              key: scaffoldState,
              backgroundColor: colorBackground,
              appBar: AppBar(
                elevation: 0,
                backgroundColor: colorBackground,
                title: Text(
                  "${trend.judul}",
                  style: TextStyle(
                      color: colorPrimary,
                      fontWeight: FontWeight.bold,
                      fontSize: size.height <= 600 ? 18 : 20),
                ),
                centerTitle: true,
                leading: IconButton(
                  icon: FaIcon(
                    FontAwesomeIcons.chevronLeft,
                    color: colorPrimary,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              body: Stack(
                children: [mainBody(size), scrollBottomSheet(size)],
              ),
              bottomSheet: buildTextComment(size),
            ),
    );
  }

  // Main Body builder
  Widget mainBody(Size size) {
    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Container(
              height: size.height <= 600 ? 150 : 200,
              width: size.width,
              color: colorDarkNeutral,
              child: Image.network(
                "https://traderindo.com/${trend.image}",
                fit: BoxFit.cover,
              ),
            ),
            Container(
              width: size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    "${trend.user.fullname}",
                    style: TextStyle(
                        color: colorDarkNeutral,
                        fontWeight: FontWeight.w700,
                        fontSize: 14),
                  ),
                  Text(
                    "${formatter.format(trend.date)} (${trend.readTime} mins)",
                    style: TextStyle(
                        color: colorDarkNeutral,
                        fontWeight: FontWeight.w500,
                        fontSize: 14),
                  ),
                ],
              ),
            ),
            Container(
              padding: size.width < 500
                  ? EdgeInsets.all(20)
                  : EdgeInsets.symmetric(vertical: 20, horizontal: 40),
              child: Html(
                data: trend.artikel,
                style: {
                  "p": Style(
                      textAlign: TextAlign.justify,
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontSize: FontSize(14))
                },
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: LongButton(
                size: size,
                title: "RESULT",
                bgColor: colorPrimary,
                textColor: colorBackground,
                onClick: () {
                  print("jalan");
                  if (_controller.isDismissed) {
                    _controller.forward();
                  } else if (_controller.isCompleted) {
                    _controller.reverse();
                  }
                },
              ),
            ),
            SizedBox(height: 20),
            Container(
              padding:
                  EdgeInsets.symmetric(horizontal: size.width < 500 ? 20 : 40),
              width: size.width,
              child: Row(
                children: [
                  ColumnItem(
                    width: size.width * 0.45,
                    title: "Pair",
                    subtitle: "${trend.pair.toUpperCase()}",
                  ),
                  ColumnItem(
                    width: size.width * 0.4,
                    title: "Action",
                    subtitle: "$action",
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Container(
              padding:
                  EdgeInsets.symmetric(horizontal: size.width < 500 ? 20 : 40),
              width: size.width,
              child: Row(
                children: [
                  ColumnItem(
                    width: size.width * 0.45,
                    title: "Take Profit",
                    subtitle: "${trend.takeprofit}",
                  ),
                  ColumnItem(
                    width: size.width * 0.4,
                    title: "Stop Loss",
                    subtitle: "${trend.stoploss}",
                  ),
                ],
              ),
            ),
            SizedBox(height: 30),

            // State listener after doing a reaction
            BlocListener<TrendingDetailBloc, TrendingDetailState>(
              listener: (ctx, state) {
                if (state is TrendingDetailStateFailReact) {
                  Util().showToast(
                      msg: "Something wrong !",
                      color: colorError,
                      txtColor: colorBackground,
                      context: this.context);
                }
              },
              child: Container(
                width: size.width,
                padding: EdgeInsets.symmetric(
                    horizontal: size.width < 500 ? 15 : 40),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        reactSignal(reaction: "love");
                      },
                      child: EmojiCounter(
                        emoji: "assets/emoji/lovy.png",
                        counter: "${trend.totalLove}",
                        size: size,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        reactSignal(reaction: "happy");
                      },
                      child: EmojiCounter(
                        emoji: "assets/emoji/tiup.png",
                        counter: "${trend.totalHappy}",
                        size: size,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        reactSignal(reaction: "sad");
                      },
                      child: EmojiCounter(
                        emoji: "assets/emoji/heh.png",
                        counter: "${trend.totalSad}",
                        size: size,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        reactSignal(reaction: "angry");
                      },
                      child: EmojiCounter(
                        emoji: "assets/emoji/angry.png",
                        counter: "${trend.totalAngry}",
                        size: size,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        reactSignal(reaction: "confused");
                      },
                      child: EmojiCounter(
                        emoji: "assets/emoji/bruh.png",
                        counter: "${trend.totalConfused}",
                        size: size,
                      ),
                    )
                  ],
                ),
              ),
            ),

            SizedBox(height: 30),

            // Show all comment
            BlocListener<CommentBloc, CommentState>(
              listener: (context, state) {
                print("state");
                print(state);
                if (state is CommentStateSuccessLoad) {
                  setState(() {
                    comentList = state.comments;
                    textComment.text = "";
                  });
                }
              },
              child: comentList.length == 0
                  ? Container()
                  : Container(
                      padding: size.width < 500
                          ? EdgeInsets.zero
                          : EdgeInsets.symmetric(horizontal: 40),
                      child: Column(
                        children: [
                          ...comentList
                              .map((data) => buildComment(size, comments: data))
                        ],
                      )),
              // buildComment(size), buildComment(size)
            ),
            SizedBox(height: 60),
          ],
        ));
  }

  // Show scrollable sheet
  Widget scrollBottomSheet(Size size) {
    return Container(
      child: SlideTransition(
        position: _tween.animate(_controller),
        child: DraggableScrollableSheet(
          initialChildSize: 0.5,
          maxChildSize: 0.9,
          minChildSize: 0.2,
          builder: (context, scrollController) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              decoration: BoxDecoration(
                  color: colorBackground,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 20,
                        offset: Offset(0, -4),
                        color: Color(0xFF505050).withOpacity(0.5))
                  ]),
              child: ListView(
                controller: scrollController,
                children: [
                  Align(
                    child: Container(
                        height: 5,
                        width: 50,
                        decoration: BoxDecoration(
                          color: colorDarkNeutral,
                          borderRadius: BorderRadius.circular(20),
                        )),
                  ),
                  SizedBox(height: 20),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding:
                              EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                          decoration: BoxDecoration(
                              color: colorSecondaryRed,
                              borderRadius: BorderRadius.circular(6)),
                          child: Text(
                            "STOP LOSS",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: colorBackground,
                                fontWeight: FontWeight.w700,
                                fontSize: 11),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            _controller.reverse();
                          },
                          child: FaIcon(
                            FontAwesomeIcons.times,
                            color: colorSecondaryRed,
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RichText(
                            text: TextSpan(children: [
                          TextSpan(
                              text: "SELL",
                              style: TextStyle(
                                  color: colorSemanticRed,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 24)),
                          TextSpan(
                              text: " XA4H11",
                              style: TextStyle(
                                  color: colorSemanticBlack,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 24)),
                        ])),
                        Text("-7.25%",
                            style: TextStyle(
                                color: colorSemanticRed,
                                fontWeight: FontWeight.bold,
                                fontSize: 24)),
                      ],
                    ),
                  ),
                  SizedBox(height: 16),
                  ModalBottomSheetItem(
                    text1: "ID",
                    text2: "bb23r88120ue",
                  ),
                  SizedBox(height: 10),
                  ModalBottomSheetItem(
                    text1: "Open Time",
                    text2: "2019.12.17 08:40:47",
                  ),
                  SizedBox(height: 10),
                  ModalBottomSheetItem(
                    text1: "Open Price",
                    text2: "1.53324",
                  ),
                  SizedBox(height: 10),
                  ModalBottomSheetItem(
                    text1: "S/L",
                    text2: "1.12341",
                  ),
                  SizedBox(height: 10),
                  ModalBottomSheetItem(
                    text1: "T/P",
                    text2: "1.33421",
                  ),
                  SizedBox(height: 10),
                  ModalBottomSheetItem(
                    text1: "Lots",
                    text2: "100.00",
                  ),
                  SizedBox(height: 10),
                  ModalBottomSheetItem(
                    text1: "Close Time",
                    text2: "2019.12.17 08:40:47",
                  ),
                  SizedBox(height: 25),
                  ModalBottomSheetItem(
                    text1: "Close Price",
                    text2: "1.112144",
                  ),
                  SizedBox(height: 10),
                  ModalBottomSheetItem(
                    text1: "Pips",
                    text2: "-15.6",
                  ),
                  SizedBox(height: 10),
                  ModalBottomSheetItem(
                    text1: "Duration",
                    text2: "00h01m",
                  ),
                  SizedBox(height: 10),
                  ModalBottomSheetItem(
                    text1: "Profit",
                    text2: "-15600",
                  ),
                  SizedBox(height: 50),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  // Function to handle posting comment
  void postComment() {
    if (textComment.text != "" || textComment.text != null) {
      BlocProvider.of<CommentBloc>(context).add(CommentEventPostComment(
          signalID: widget.id, comment: textComment.text));
    }
  }

  // Function to handle reply comment
  void replyComment() {
    if ((textComment.text != "" || textComment.text != null) &&
        parrentCommentID != null) {
      BlocProvider.of<CommentBloc>(context).add(CommentEventReplyComment(
        commentID: parrentCommentID,
        signalID: widget.id,
        comment: textComment.text,
      ));
    }
  }

  // Function to handle vote comment
  void voteComment({String reaction, int commentID, bool child: false}) {
    BlocProvider.of<CommentBloc>(context).add(CommentEventVote(
        signalID: widget.id,
        reaction: reaction,
        commentID: commentID,
        child: child));
  }

  // Function to handle reaction
  void reactSignal({String reaction}) {
    BlocProvider.of<TrendingDetailBloc>(context).add(
        TrendingDetailEventAddReaction(
            reaction: reaction, signalID: widget.id));
  }

  // Widget to build text comment and reply
  Widget buildTextComment(Size size) {
    return AnimatedContainer(
      color: colorNeutral30,
      height: parrentCommentID == null ? 50 : 90,
      duration: Duration(milliseconds: 600),
      curve: Curves.fastOutSlowIn,
      child: Column(
        children: [
          AnimatedContainer(
            height: parrentCommentID == null ? 0 : 40,
            duration: Duration(milliseconds: 600),
            curve: Curves.fastOutSlowIn,
            color: colorNeutral50,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Replying to @$parrentUsername",
                      style: TextStyle(
                          color: colorDarkNeutral,
                          fontSize: 15,
                          fontWeight: FontWeight.w500)),
                  parrentCommentID == null
                      ? Container()
                      : InkWell(
                          onTap: () {
                            setState(() {
                              parrentCommentID = null;
                              parrentUsername = null;
                            });
                          },
                          child: FaIcon(
                            FontAwesomeIcons.times,
                            color: colorDarkNeutral,
                          ),
                        ),
                ],
              ),
            ),
          ),
          Container(
            width: size.width,
            child: Stack(
              children: [
                Container(
                  width: size.width * 0.8,
                  height: 50,
                  padding: EdgeInsets.only(left: 10, bottom: 10),
                  child: ConstrainedBox(
                      constraints: BoxConstraints(maxHeight: 300),
                      child: TextFormField(
                          focusNode: focusReply,
                          maxLines: null,
                          style: TextStyle(
                              fontSize: 16, fontFamily: "Open-Sans", height: 1),
                          controller: textComment,
                          textInputAction: TextInputAction.done,
                          keyboardType: TextInputType.text,
                          cursorHeight: 10,
                          decoration: InputDecoration(
                            hintText: "type something . . .",
                            hintStyle: TextStyle(
                                color: colorNeutral,
                                fontSize: 15,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.italic),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                          ))),
                ),
                Positioned(
                  right: 0,
                  bottom: 0,
                  child: TextButton(
                      onPressed:
                          parrentCommentID == null ? postComment : replyComment,
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 30, vertical: 16),
                        child: Center(
                          child: Text(
                            "Post",
                            style: TextStyle(
                                color: colorPrimary,
                                fontWeight: FontWeight.w700,
                                fontSize: 16),
                          ),
                        ),
                      )),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  // Comment Builder
  Widget buildComment(Size size, {CommentModel comments}) {
    // sort reply comment list
    if (comments != null && comments.child.length > 0) {
      comments.child.sort((x, y) => y.like.compareTo(x.like));
    }
    return Column(
      children: [
        CommentItem(
          size: size,
          username: "${comments.userName}",
          time: "1 h",
          comment: "${comments.comment}",
          voteDown: "${comments.dislike}",
          voteUp: "${comments.like}",
          callbackVU: () {
            voteComment(reaction: "like", commentID: comments.id);
          },
          callbackVD: () {
            voteComment(reaction: "dislike", commentID: comments.id);
          },
          clickReply: () {
            focusReply.requestFocus();
            setState(() {
              parrentCommentID = comments.id;
              parrentUsername = comments.userName;
            });
          },
        ),
        comments.child == null || comments.child.length == 0
            ? Container()
            : Container(
                padding: EdgeInsets.only(left: 20),
                child: Column(
                  children: [
                    ...comments.child.map((val) => CommentItem(
                          size: size,
                          child: true,
                          username: "${val.userName}",
                          time: "45 m",
                          comment: "${val.comment}",
                          voteDown: "${val.dislike}",
                          voteUp: "${val.like}",
                          callbackVU: () {
                            voteComment(
                                reaction: "like",
                                commentID: val.id,
                                child: true);
                          },
                          callbackVD: () {
                            voteComment(
                                reaction: "dislike",
                                commentID: val.id,
                                child: true);
                          },
                          clickReply: () {
                            focusReply.requestFocus();
                            setState(() {
                              parrentCommentID = val.commentID;
                              parrentUsername = comments.userName;
                            });
                          },
                        ))
                  ],
                )),
      ],
    );
  }
}
