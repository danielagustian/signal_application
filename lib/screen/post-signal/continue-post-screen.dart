import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/services/filter-service.dart';

class ContinuPostSignalScreen extends StatefulWidget {
  const ContinuPostSignalScreen({Key key, this.word, this.image})
      : super(key: key);

  @override
  _ContinuPostSignalScreenState createState() =>
      _ContinuPostSignalScreenState();

  final String word;
  final File image;
}

class _ContinuPostSignalScreenState extends State<ContinuPostSignalScreen> {
  // For data action and pair
  FilterService _filterService = FilterService();
  List actionList = [];
  List pairList = [];

  // var for choosed
  String choosedAct;
  String choosedPair;

  // function to get all the filter
  void getFilterList() async {
    var actResult = await _filterService.getFilterCategory(category: "action");
    var pairResult = await _filterService.getFilterCategory(category: "pairs");

    setState(() {
      actionList = actResult;
      pairList = pairResult;
      // choosedAct = actResult[0];
      // choosedPair = pairResult[0];
    });
  }

  // Text Controller
  TextEditingController textTP = TextEditingController();
  TextEditingController textSL = TextEditingController();

  @override
  void initState() {
    super.initState();
    getFilterList();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: colorBackground,
        title: Text(
          "Post Signal",
          style: TextStyle(
              color: colorSemanticBlack,
              fontWeight: FontWeight.w500,
              fontSize: size.height <= 600 ? 18 : 22),
        ),
        leading: IconButton(
          icon: FaIcon(
            FontAwesomeIcons.chevronLeft,
            color: colorPrimary,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        actions: [
          Center(
            child: InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.only(right: 16),
                child: Text(
                  "Post",
                  style: TextStyle(
                      color: colorPrimary,
                      fontWeight: FontWeight.w700,
                      fontSize: size.height <= 600 ? 12 : 14),
                ),
              ),
            ),
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: pairList.length == 0
            ? Center(
                child: CircularProgressIndicator(
                backgroundColor: colorBackground,
                valueColor: AlwaysStoppedAnimation(colorPrimary),
              ))
            : ListView(
                scrollDirection: Axis.vertical,
                children: [
                  Container(
                    width: size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Pair",
                          style: TextStyle(
                              color: colorPrimary,
                              fontWeight: FontWeight.w700,
                              fontSize: size.height <= 600 ? 14 : 16),
                        ),
                        chooserItem(size,
                            title: "Select Pair", choosed: choosedPair),
                      ],
                    ),
                  ),
                  Container(
                    width: size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Action",
                          style: TextStyle(
                              color: colorPrimary,
                              fontWeight: FontWeight.w700,
                              fontSize: size.height <= 600 ? 14 : 16),
                        ),
                        chooserItem(size,
                            title: "Select Action", choosed: choosedAct),
                      ],
                    ),
                  ),
                  Container(
                      child: Stack(
                    alignment: Alignment.centerLeft,
                    children: [
                      Builder(builder: (context) {
                        return Text(
                          'Take Profit',
                          style: TextStyle(
                              color: colorPrimary,
                              fontWeight: FontWeight.bold,
                              fontSize: size.height <= 600 ? 12 : 14),
                        );
                      }),
                      TextFormField(
                        controller: textTP,
                        textAlign: TextAlign.right,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.number,
                        style: TextStyle(fontFamily: "Open-Sans"),
                        decoration: InputDecoration(
                            hintText: "Enter TP",
                            hintStyle: TextStyle(color: colorPrimary),
                            contentPadding:
                                EdgeInsets.only(left: 50, right: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none),
                      ),
                    ],
                  )),
                  Container(
                      child: Stack(
                    alignment: Alignment.centerLeft,
                    children: [
                      Builder(builder: (context) {
                        return Text(
                          'Stop Loss',
                          style: TextStyle(
                              color: colorPrimary,
                              fontWeight: FontWeight.bold,
                              fontSize: size.height <= 600 ? 12 : 14),
                        );
                      }),
                      TextFormField(
                        controller: textSL,
                        textAlign: TextAlign.right,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.number,
                        style: TextStyle(fontFamily: "Open-Sans"),
                        decoration: InputDecoration(
                            hintText: "Enter SL",
                            hintStyle: TextStyle(color: colorPrimary),
                            contentPadding:
                                EdgeInsets.only(left: 50, right: 10),
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none),
                      ),
                    ],
                  )),
                ],
              ),
      ),
    );
  }

  Widget chooserItem(Size size, {String title, String choosed}) {
    List itemList = pairList;
    if (title == "Select Action") {
      itemList = actionList;
    }

    String showed = choosed;
    if (choosed == "OP_BUY") {
      showed = "BUY";
    } else if (choosed == "OP_SELL") {
      showed = "SELL";
    } else if (choosed == "OP_SELLLIMIT") {
      showed = "SELL LIMIT";
    }

    return Container(
      child: DropdownButton<String>(
        icon: FaIcon(
          FontAwesomeIcons.chevronRight,
          color: colorPrimary,
          size: 20,
        ),
        underline: Container(),
        hint: Container(
          margin: EdgeInsets.only(right: 5, bottom: 2),
          child: Text(
            choosed != null ? showed.toUpperCase() : title,
            style: TextStyle(
                color: colorPrimary, fontSize: size.height <= 600 ? 12 : 14),
          ),
        ),
        elevation: 16,
        style: TextStyle(color: colorPrimary),
        onChanged: (String newValue) {
          setState(() {
            if (title == "Select Action") {
              choosedAct = newValue;
            } else {
              choosedPair = newValue;
            }
          });
        },
        items: itemList.map<DropdownMenuItem<String>>((value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      ),
    );
  }
}
