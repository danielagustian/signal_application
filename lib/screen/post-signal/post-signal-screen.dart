// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:html_editor_enhanced/html_editor.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:signal_app/constant/constant.dart';
// import 'package:signal_app/screen/post-signal/continue-post-screen.dart';

// class PostSignalScreen extends StatefulWidget {
//   @override
//   _PostSignalScreenState createState() => _PostSignalScreenState();
// }

// class _PostSignalScreenState extends State<PostSignalScreen> {
//   // Summernote key
//   // GlobalKey<FlutterSummernoteState> _keyEditor = GlobalKey();
//   String word = "";
//   // Image picker
//   File _image;
//   final picker = ImagePicker();

//   Future getImage() async {
//     final pickedFile = await picker.getImage(source: ImageSource.gallery);

//     setState(() {
//       if (pickedFile != null) {
//         _image = File(pickedFile.path);
//       } else {
//         print('No image selected.');
//       }
//     });
//   }

//   // Controller HTML
//   HtmlEditorController htmlController = HtmlEditorController();

//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;

//     return Scaffold(
//       appBar: AppBar(
//         elevation: 0,
//         backgroundColor: colorBackground,
//         title: Text(
//           "Post Signal",
//           style: TextStyle(
//               color: colorSemanticBlack,
//               fontWeight: FontWeight.w500,
//               fontSize: size.height <= 600 ? 18 : 22),
//         ),
//         centerTitle: true,
//         automaticallyImplyLeading: false,
//         actions: [
//           Center(
//             child: InkWell(
//               onTap: () async {
//                 final val = await htmlController.getText();
//                 setState(() {
//                   word = val;
//                 });
//                 if ((val != "" || val != null) && _image != null)
//                   Navigator.push(
//                       context,
//                       MaterialPageRoute(
//                           builder: (context) => ContinuPostSignalScreen(
//                                 word: word,
//                                 image: _image,
//                               )));
//               },
//               child: Container(
//                 margin: EdgeInsets.only(right: 16),
//                 child: Text(
//                   "Next",
//                   style: TextStyle(
//                       color: colorPrimary,
//                       fontWeight: FontWeight.w700,
//                       fontSize: size.height <= 600 ? 12 : 14),
//                 ),
//               ),
//             ),
//           )
//         ],
//       ),
//       body: SingleChildScrollView(
//         child: SafeArea(
//           child: Column(
//             children: [
//               InkWell(
//                 onTap: getImage,
//                 child: Container(
//                   margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
//                   height: 200,
//                   width: size.width,
//                   decoration: BoxDecoration(
//                     color: colorNeutral30,
//                     borderRadius: BorderRadius.circular(10),
//                   ),
//                   child: ClipRRect(
//                     borderRadius: BorderRadius.circular(10),
//                     child: _image == null
//                         ? Center(
//                             child: Container(
//                             child: FaIcon(
//                               FontAwesomeIcons.camera,
//                               color: colorDarkNeutral,
//                             ),
//                           ))
//                         : Image.file(
//                             _image,
//                             fit: BoxFit.cover,
//                           ),
//                   ),
//                 ),
//               ),
//               HtmlEditor(
//                 controller: htmlController,
//                 options: HtmlEditorOptions(
//                     height: size.height * 0.7, shouldEnsureVisible: true),
//                 toolbar: [
//                   Font(buttons: [
//                     FontButtons.bold,
//                     FontButtons.underline,
//                     FontButtons.italic,
//                     FontButtons.clear
//                   ]),
//                   Paragraph(buttons: [ParagraphButtons.ol])
//                 ],
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_summernote/flutter_summernote.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:signal_app/component/long-button.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/screen/post-signal/continue-post-screen.dart';

class PostSignalScreen extends StatefulWidget {
  @override
  _PostSignalScreenState createState() => _PostSignalScreenState();
}

class _PostSignalScreenState extends State<PostSignalScreen> {
  // Summernote key
  GlobalKey<FlutterSummernoteState> _keyEditor = GlobalKey();
  String word = "";

  // Image picker
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: colorBackground,
        title: Text(
          "Post Signal",
          style: TextStyle(
              color: colorSemanticBlack,
              fontWeight: FontWeight.w500,
              fontSize: size.height <= 600 ? 18 : 22),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        actions: [
          Center(
            child: InkWell(
              onTap: () async {
                final val = await _keyEditor.currentState?.getText();
                setState(() {
                  word = val;
                });

                if ((val != "" || val != null) && _image != null)
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ContinuPostSignalScreen(
                                word: word,
                                image: _image,
                              )));
              },
              child: Container(
                margin: EdgeInsets.only(right: 16),
                child: Text(
                  "Next",
                  style: TextStyle(
                      color: colorPrimary,
                      fontWeight: FontWeight.w700,
                      fontSize: size.height <= 600 ? 12 : 14),
                ),
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              InkWell(
                onTap: getImage,
                child: Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
                  height: 200,
                  width: size.width,
                  decoration: BoxDecoration(
                    color: colorNeutral30,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: _image == null
                        ? Center(
                            child: Container(
                            child: FaIcon(
                              FontAwesomeIcons.camera,
                              color: colorDarkNeutral,
                            ),
                          ))
                        : Image.file(
                            _image,
                            fit: BoxFit.cover,
                          ),
                  ),
                ),
              ),
              FlutterSummernote(
                key: _keyEditor,
                hasAttachment: true,
                customToolbar: """
                  [  
                    ['style', ['bold', 'italic', 'underline', 'clear']],  
                    ['para', ['ul']], 
                  ]
                """,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
