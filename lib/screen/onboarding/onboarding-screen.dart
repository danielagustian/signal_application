import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/screen/authentication/login-screen.dart';

class OnBoardingScreen extends StatefulWidget {
  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  PageController _controller = PageController(initialPage: 0);
  int pageIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  void haveInstalledApp() async {
    // if user have been install the app
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt("install", 1);
  }

  void moveToLogin() {
    // After this user will not get show the onboarding until re install app
    haveInstalledApp();
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            // if user has been login
            builder: (context) => LoginScreen()));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: colorBackground,
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              alignment: Alignment.centerRight,
              child: InkWell(
                onTap: () {
                  _controller.animateToPage(2,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.linear);
                },
                child: Text(
                  "Skip",
                  style: TextStyle(
                      color: colorPrimary,
                      fontSize: size.height < 600 ? 22 : 25),
                ),
              ),
            ),
            Container(
              width: size.width,
              height: size.height * 0.6,
              child: PageView.builder(
                controller: _controller,
                onPageChanged: (value) {
                  setState(() {
                    pageIndex = value;
                  });
                },
                itemCount: 3,
                itemBuilder: (context, index) {
                  return Container(
                    child: size.width < 500
                        ? onboardingCard(size)
                        : SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: onboardingCard(size)),
                          ),
                  );
                },
              ),
            ),
            Container(
              width: size.width,
              height: 20,
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: animateIndicator(),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: InkWell(
                      onTap: () {
                        if (pageIndex != 2) {
                          _controller.animateToPage(pageIndex + 1,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.linear);
                        } else {
                          if (pageIndex == 2) {
                            moveToLogin();
                          } else {
                            _controller.animateToPage(2,
                                duration: Duration(milliseconds: 500),
                                curve: Curves.linear);
                          }
                        }
                      },
                      child: pageIndex == 2
                          ? Text(
                              "Sign In",
                              style:
                                  TextStyle(color: colorPrimary, fontSize: 18),
                            )
                          : Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "Next",
                                  style: TextStyle(
                                      color: colorPrimary, fontSize: 18),
                                ),
                                SizedBox(width: 5),
                                FaIcon(
                                  FontAwesomeIcons.arrowRight,
                                  color: colorPrimary,
                                )
                              ],
                            ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Column onboardingCard(Size size) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CircleAvatar(
          backgroundColor: colorSemanticGreen,
          radius: size.height < 600 ? 90 : 140,
        ),
        SizedBox(height: 20),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          alignment: Alignment.centerLeft,
          child: Text(
            "LATEST UPDATE",
            style: TextStyle(
                color: colorPrimary,
                fontWeight: FontWeight.bold,
                fontSize: size.height < 600 ? 18 : 20),
          ),
        ),
        SizedBox(height: 10),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          alignment: Alignment.centerLeft,
          child: Text(
            "Lorem ipsum dolor sit amet consecr adipiscing elit.",
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: size.height < 600 ? 18 : 20),
          ),
        ),
      ],
    );
  }

  // Animated indicator
  Widget animateIndicator() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          height: 10,
          width: pageIndex == 0 ? 50 : 10,
          decoration: BoxDecoration(
              color:
                  pageIndex == 0 ? colorPrimary : colorPrimary.withOpacity(0.5),
              borderRadius: BorderRadius.circular(20)),
        ),
        SizedBox(width: 5),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          height: 10,
          width: pageIndex == 1 ? 50 : 10,
          decoration: BoxDecoration(
              color: pageIndex == 1 ? colorPrimary : colorPrimary50,
              borderRadius: BorderRadius.circular(20)),
        ),
        SizedBox(width: 5),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          height: 10,
          width: pageIndex == 2 ? 50 : 10,
          decoration: BoxDecoration(
              color:
                  pageIndex == 2 ? colorPrimary : colorPrimary.withOpacity(0.5),
              borderRadius: BorderRadius.circular(20)),
        )
      ],
    );
  }
}
