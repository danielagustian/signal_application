import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:signal_app/bloc/authentication/auth-bloc.dart';
import 'package:signal_app/bloc/authentication/auth-event.dart';
import 'package:signal_app/bloc/authentication/auth-state.dart';
import 'package:signal_app/component/long-button-icon.dart';
import 'package:signal_app/component/long-button.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/screen/authentication/register-screen.dart';
import 'package:signal_app/screen/tab-layout.dart';
import 'package:signal_app/utils/util.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController textEmail = new TextEditingController();
  TextEditingController textPassword = new TextEditingController();
  bool _emailValidator = false;
  bool _passValidator = false;
  bool _obscureText = true;
  bool dontShowAgain = false;
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (context, state) {
        //Success Login
        if (state is AuthStateSuccessLoad) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => MainTabLayout()),
          );
          setState(() {
            loading = false;
          });
          //Failed Login
        } else if (state is AuthStateFailLoad) {
          setState(() {
            loading = false;
          });
          Util().showToast(
              context: this.context,
              msg: "Something Wrong !",
              color: colorError,
              txtColor: colorBackground);
        }
      },
      child: Scaffold(
        backgroundColor: colorBackground,
        body: Center(
            child: SingleChildScrollView(
          child: Container(
              padding: size.width < 500
                  ? EdgeInsets.all(20)
                  : EdgeInsets.symmetric(vertical: 20, horizontal: 50),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Welcome Back ",
                      style: TextStyle(fontSize: size.height < 569 ? 16 : 20),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Text("Login to Your Account",
                          style: TextStyle(
                              fontSize: size.height < 569 ? 28 : 30,
                              fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(height: 30),
                    Text("Email",
                        style: TextStyle(
                            fontSize: size.height < 569 ? 14 : 18,
                            color: Color(0xFF4A5568))),
                    SizedBox(height: size.height * 0.02),
                    Container(
                      height: 45,
                      child: TextFormField(
                        style: TextStyle(fontFamily: "Open-Sans"),
                        controller: textEmail,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.only(bottom: 4, right: 8, left: 8),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              borderSide: BorderSide(
                                  color: _emailValidator
                                      ? colorError
                                      : colorPrimary.withOpacity(0.3),
                                  width: 2.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(
                                  color: _emailValidator
                                      ? colorError
                                      : Color(0xFFE8E8E8),
                                  width: 2.0,
                                ))),
                      ),
                    ),
                    SizedBox(
                      height: size.height * 0.025,
                    ),
                    Text("Password",
                        style: TextStyle(
                            fontSize: size.height < 569 ? 14 : 18,
                            color: Color(0xFF4A5568))),
                    SizedBox(height: size.height * 0.02),
                    Container(
                      height: 45,
                      child: TextFormField(
                          style: TextStyle(fontFamily: "Open-Sans"),
                          obscureText: _obscureText,
                          controller: textPassword,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.only(bottom: 4, right: 8, left: 8),
                              suffixIcon: IconButton(
                                icon: FaIcon(
                                  _obscureText
                                      ? FontAwesomeIcons.solidEye
                                      : FontAwesomeIcons.solidEyeSlash,
                                  color: colorNeutral,
                                ),
                                onPressed: () {
                                  setState(() {
                                    _obscureText = !_obscureText;
                                  });
                                },
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(
                                    color: _passValidator
                                        ? colorError
                                        : colorPrimary.withOpacity(0.3),
                                    width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(
                                  color: _passValidator
                                      ? colorError
                                      : Color(0xFFE8E8E8),
                                  width: 1.0,
                                ),
                              ))),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: size.width * 0.5,
                            child: Align(
                                alignment: Alignment.topLeft,
                                child: CheckboxListTile(
                                  contentPadding: EdgeInsets.all(0),
                                  title: Text(
                                    "Remember Me",
                                    style: TextStyle(fontSize: 14),
                                  ),
                                  value: dontShowAgain,
                                  onChanged: _onDontShowAgainChanged,
                                  controlAffinity:
                                      ListTileControlAffinity.leading,
                                ))),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => RegisterScreen()));
                          },
                          child: Text("Forgot Password?"),
                        )
                      ],
                    ),
                    SizedBox(
                      height: size.height * 0.03,
                    ),
                    Center(
                      child: LongButton(
                        size: size,
                        onClick: onLogin,
                        bgColor: colorPrimary,
                        textColor: colorNeutral,
                        title: "LOGIN",
                        loading: loading,
                      ),
                    ),
                    SizedBox(
                      height: size.height * 0.03,
                    ),
                    Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              height: 1,
                              width: size.width * 0.4,
                              color: colorDarkNeutral),
                          Text("or",
                              style: TextStyle(
                                  height: 0.4,
                                  fontWeight: FontWeight.w600,
                                  fontSize: size.height < 569 ? 12 : 14,
                                  color: colorDarkNeutral)),
                          Container(
                              height: 1,
                              width: size.width * 0.4,
                              color: colorDarkNeutral)
                        ],
                      ),
                    ),
                    SizedBox(
                      height: size.height * 0.03,
                    ),
                    Center(
                      child: LongButtonIcon(
                          size: size,
                          title: "SIGN IN WITH GOOGLE",
                          bgColor: colorGoogle,
                          textColor: colorBackground,
                          iconWidget: Image.asset(
                              'assets/images/google-icon.png',
                              height: size.height * 0.04,
                              width: size.height * 0.04),
                          onClick: () {
                            print("Log In With Google");
                          }),
                    ),
                    SizedBox(
                      height: size.height * 0.05,
                    ),
                    Container(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Don't have any account ?",
                            style: TextStyle(
                                fontSize: size.height < 569 ? 11 : 12,
                                color: colorSemanticBlack)),
                        SizedBox(width: 5),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => RegisterScreen()));
                          },
                          child: Text("Sign Up",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: size.height < 569 ? 11 : 12,
                                  color: colorPrimary)),
                        ),
                      ],
                    )),
                  ])),
        )),
      ),
    );
  }

  void onLogin() {
    setState(() {
      loading = true;
    });
    if (textEmail.text == "") {
      setState(() {
        _emailValidator = true;
      });
    } else {
      setState(() {
        _emailValidator = false;
      });
    }
    if (textPassword.text == "") {
      setState(() {
        _passValidator = true;
      });
    } else {
      setState(() {
        _passValidator = false;
      });
    }

    if (!_emailValidator && !_passValidator) {
      BlocProvider.of<AuthenticationBloc>(context).add(AuthEventLoginForm(
          email: textEmail.text,
          password: textPassword.text,
          show: dontShowAgain));
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  void _onDontShowAgainChanged(bool newValue) {
    setState(() {
      dontShowAgain = newValue;

      if (dontShowAgain) {
        print("I wont show again. Sorry to bother you.");
      } else {
        print("We will meet again");
      }
    });
  }
}
