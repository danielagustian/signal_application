import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:signal_app/bloc/authentication/auth-bloc.dart';
import 'package:signal_app/bloc/authentication/auth-event.dart';
import 'package:signal_app/bloc/authentication/auth-state.dart';
import 'package:signal_app/component/long-button-icon.dart';
import 'package:signal_app/component/long-button.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/models/user-model.dart';
import 'package:signal_app/screen/authentication/finish-register-screen.dart';
import 'package:signal_app/screen/tab-layout.dart';
import 'package:signal_app/utils/util.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController textEmail = new TextEditingController();
  TextEditingController textPassword = new TextEditingController();
  TextEditingController textName = new TextEditingController();
  TextEditingController textConfirmPassword = new TextEditingController();
  bool _passValidator = false;
  bool _emailValidator = false;
  bool _nameValidator = false;
  bool _confirmValidator = false;
  bool _obscureText = true;
  bool _obscureText2 = true;
  bool dontShowAgain = false;
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorBackground,
      body: BlocListener<AuthenticationBloc, AuthenticationState>(
        listener: (context, state) {
          //Success Login
          if (state is AuthStateFailLoad) {
            setState(() {
              loading = false;
            });
            Util().showToast(
                context: this.context,
                msg: "Something Wrong !",
                color: colorError,
                txtColor: colorBackground);
          } else if (state is AuthStateFailEmail) {
            setState(() {
              _emailValidator = true;
            });
          }
        },
        child: Center(
            child: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Do you have an account ? ",
                      style: TextStyle(fontSize: size.height <= 600 ? 16 : 20),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                      child: Text("Sign Up",
                          style: TextStyle(
                              fontSize: size.height <= 600 ? 24 : 30,
                              fontWeight: FontWeight.bold)),
                    ),
                    Text("Fullname",
                        style: TextStyle(
                            fontSize: size.height <= 600 ? 14 : 18,
                            color: Color(0xFF4A5568))),
                    SizedBox(height: size.height * 0.015),
                    Container(
                      height: size.height <= 600 ? 35 : 40,
                      child: TextFormField(
                        style: TextStyle(fontFamily: "Open-Sans"),
                        controller: textName,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.only(bottom: 4, left: 10, right: 10),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              borderSide: BorderSide(
                                  color: _nameValidator
                                      ? colorError
                                      : colorPrimary.withOpacity(0.3),
                                  width: 2.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(
                                  color: _nameValidator
                                      ? colorError
                                      : Color(0xFFE8E8E8),
                                  width: 2.0,
                                ))),
                      ),
                    ),
                    SizedBox(
                      height: size.height * 0.01,
                    ),
                    Text("Email",
                        style: TextStyle(
                            fontSize: size.height <= 600 ? 14 : 18,
                            color: Color(0xFF4A5568))),
                    SizedBox(height: size.height * 0.015),
                    Container(
                      height: size.height <= 600 ? 35 : 40,
                      child: TextFormField(
                        style: TextStyle(fontFamily: "Open-Sans"),
                        controller: textEmail,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.only(bottom: 4, left: 10, right: 10),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              borderSide: BorderSide(
                                  color: _emailValidator
                                      ? colorError
                                      : colorPrimary.withOpacity(0.3),
                                  width: 2.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(
                                  color: _emailValidator
                                      ? colorError
                                      : Color(0xFFE8E8E8),
                                  width: 2.0,
                                ))),
                      ),
                    ),
                    SizedBox(
                      height: size.height * 0.01,
                    ),
                    Text("Password",
                        style: TextStyle(
                            fontSize: size.height <= 600 ? 14 : 18,
                            color: Color(0xFF4A5568))),
                    SizedBox(height: size.height * 0.015),
                    Container(
                      height: size.height <= 600 ? 35 : 40,
                      child: TextFormField(
                          style: TextStyle(fontFamily: "Open-Sans"),
                          obscureText: _obscureText,
                          controller: textPassword,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(
                                  bottom: 4, left: 10, right: 10),
                              suffixIcon: IconButton(
                                icon: FaIcon(
                                  _obscureText
                                      ? FontAwesomeIcons.solidEye
                                      : FontAwesomeIcons.solidEyeSlash,
                                  color: colorNeutral,
                                ),
                                onPressed: () {
                                  setState(() {
                                    _obscureText = !_obscureText;
                                  });
                                },
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(
                                    color: _passValidator
                                        ? colorError
                                        : colorPrimary.withOpacity(0.3),
                                    width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(
                                  color: _passValidator
                                      ? colorError
                                      : Color(0xFFE8E8E8),
                                  width: 1.0,
                                ),
                              ))),
                    ),
                    SizedBox(
                      height: size.height * 0.01,
                    ),
                    Text("Confirm Password",
                        style: TextStyle(
                            fontSize: size.height <= 600 ? 14 : 18,
                            color: Color(0xFF4A5568))),
                    SizedBox(height: size.height * 0.015),
                    Container(
                      height: size.height <= 600 ? 35 : 40,
                      child: TextFormField(
                          style: TextStyle(fontFamily: "Open-Sans"),
                          obscureText: _obscureText2,
                          controller: textConfirmPassword,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(
                                  bottom: 4, left: 10, right: 10),
                              suffixIcon: IconButton(
                                icon: FaIcon(
                                  _obscureText2
                                      ? FontAwesomeIcons.solidEye
                                      : FontAwesomeIcons.solidEyeSlash,
                                  color: colorNeutral,
                                ),
                                onPressed: () {
                                  setState(() {
                                    _obscureText2 = !_obscureText2;
                                  });
                                },
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(
                                    color: _confirmValidator
                                        ? colorError
                                        : colorPrimary.withOpacity(0.3),
                                    width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(
                                  color: _confirmValidator
                                      ? colorError
                                      : Color(0xFFE8E8E8),
                                  width: 1.0,
                                ),
                              ))),
                    ),
                    Container(
                        width: size.width * 0.5,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: CheckboxListTile(
                              contentPadding: EdgeInsets.all(0),
                              activeColor: colorPrimary,
                              title: Text(
                                "Remember Me",
                                style: TextStyle(fontSize: 14),
                              ),
                              value: dontShowAgain,
                              onChanged: _onDontShowAgainChanged,
                              controlAffinity: ListTileControlAffinity.leading,
                            ))),
                    SizedBox(
                      height: size.height * 0.025,
                    ),
                    LongButton(
                      size: size,
                      onClick: onRegister,
                      bgColor: colorPrimary,
                      textColor: colorNeutral,
                      title: "SIGN UP",
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Already have an account ?",
                              style: TextStyle(
                                  fontSize: size.height <= 600 ? 11 : 12,
                                  color: colorSemanticBlack)),
                          SizedBox(width: 5),
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Text("Log In",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: size.height <= 600 ? 11 : 12,
                                    color: colorPrimary)),
                          ),
                        ],
                      ),
                    )
                  ])),
        )),
      ),
    );
  }

  void onRegister() {
    setState(() {
      loading = true;
    });
    if (textEmail.text == "") {
      setState(() {
        _emailValidator = true;
      });
    } else {
      setState(() {
        _emailValidator = false;
      });
    }
    if (textName.text == "") {
      setState(() {
        _nameValidator = true;
      });
    } else {
      setState(() {
        _nameValidator = false;
      });
    }

    if (textPassword.text == "") {
      setState(() {
        _passValidator = true;
      });
    } else {
      setState(() {
        _passValidator = false;
      });
    }
    if (textConfirmPassword.text == "" ||
        textConfirmPassword.text != textPassword.text) {
      setState(() {
        _confirmValidator = true;
      });
    } else {
      setState(() {
        _confirmValidator = false;
      });
    }

    if (!_emailValidator &&
        !_nameValidator &&
        !_passValidator &&
        !_confirmValidator) {
      UserModel user = UserModel(
        email: textEmail.text,
        fullname: textName.text,
        password: textPassword.text,
      );

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => FinishRegisterScreen(newUser: user)),
      );
      // BlocProvider.of<AuthenticationBloc>(context)
      //     .add(AuthEventRegisterForm(user: user));
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  void _onDontShowAgainChanged(bool newValue) {
    setState(() {
      dontShowAgain = newValue;

      if (dontShowAgain) {
        print("I wont show again. Sorry to bother you.");
      } else {
        print("We will meet again");
      }
    });
  }
}
