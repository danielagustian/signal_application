import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:signal_app/bloc/authentication/auth-bloc.dart';
import 'package:signal_app/bloc/authentication/auth-event.dart';
import 'package:signal_app/bloc/authentication/auth-state.dart';
import 'package:signal_app/component/long-button.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/models/user-model.dart';
import 'package:signal_app/screen/tab-layout.dart';
import 'package:signal_app/utils/util.dart';

class FinishRegisterScreen extends StatefulWidget {
  const FinishRegisterScreen({Key key, this.newUser}) : super(key: key);

  @override
  _FinishRegisterScreenState createState() => _FinishRegisterScreenState();

  final UserModel newUser;
}

class _FinishRegisterScreenState extends State<FinishRegisterScreen> {
  TextEditingController textUsername = new TextEditingController();
  bool _usernameValidator = false;

  bool dontShowAgain = false;
  bool loading = false;

  // if username alteady taken
  String usernameTaken = "";

  // Image picker
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorBackground,
      body: BlocListener<AuthenticationBloc, AuthenticationState>(
        listener: (context, state) {
          //Success Login
          if (state is AuthStateSuccessLoad) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => MainTabLayout()),
            );
            setState(() {
              loading = false;
            });
            //Failed Login
          } else if (state is AuthStateFailLoad) {
            setState(() {
              loading = false;
            });
            Util().showToast(
                context: this.context,
                msg: "Something Wrong !",
                color: colorError,
                txtColor: colorBackground);
          } else if (state is AuthStateFailUsername) {
            setState(() {
              loading = false;
              _usernameValidator = true;
              usernameTaken =
                  "Username already taken, please try something different";
            });
          } else if (state is AuthStateFailEmail) {
            setState(() {
              loading = false;
            });
            Navigator.pop(context);
          }
        },
        child: Center(
            child: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Set up your profile",
                      style: TextStyle(fontSize: size.height <= 600 ? 16 : 18),
                    ),
                    Text(
                      "picture and username",
                      style: TextStyle(fontSize: size.height <= 600 ? 16 : 18),
                    ),
                    SizedBox(height: size.height * 0.05),
                    InkWell(
                      onTap: () {
                        getImage();
                      },
                      child: Container(
                        width: 130,
                        height: 130,
                        decoration: BoxDecoration(
                          color: colorNeutral50,
                          borderRadius: BorderRadius.circular(100),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: _image == null
                              ? Center(
                                  child: FaIcon(FontAwesomeIcons.camera,
                                      color: colorDarkNeutral))
                              : Image.file(
                                  _image,
                                  fit: BoxFit.cover,
                                ),
                        ),
                      ),
                    ),
                    SizedBox(height: size.height * 0.05),
                    Container(
                      height: size.height <= 600 ? 38 : 45,
                      child: TextFormField(
                        style: TextStyle(fontFamily: "Open-Sans"),
                        controller: textUsername,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            hintText: "Username",
                            hintStyle: TextStyle(color: colorNeutral),
                            contentPadding:
                                EdgeInsets.only(bottom: 4, left: 10, right: 10),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              borderSide: BorderSide(
                                  color: _usernameValidator
                                      ? colorError
                                      : colorPrimary.withOpacity(0.3),
                                  width: 2.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(
                                  color: _usernameValidator
                                      ? colorError
                                      : Color(0xFFE8E8E8),
                                  width: 2.0,
                                ))),
                      ),
                    ),
                    usernameTaken == ""
                        ? Container()
                        : Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Text("$usernameTaken",
                                style:
                                    TextStyle(color: colorError, fontSize: 10)),
                          ),
                    SizedBox(height: size.height * 0.05),
                    LongButton(
                      size: size,
                      onClick: onRegister,
                      bgColor: colorPrimary,
                      textColor: colorNeutral,
                      title: "SIGN UP NOW",
                      loading: loading,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Already have an account ?",
                              style: TextStyle(
                                  fontSize: size.height <= 600 ? 11 : 12,
                                  color: colorSemanticBlack)),
                          SizedBox(width: 5),
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Text("Log In",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: size.height <= 600 ? 11 : 12,
                                    color: colorPrimary)),
                          ),
                        ],
                      ),
                    )
                  ])),
        )),
      ),
    );
  }

  void onRegister() {
    setState(() {
      loading = true;
    });

    if (textUsername.text == "") {
      setState(() {
        _usernameValidator = true;
      });
    } else {
      setState(() {
        _usernameValidator = false;
      });
    }

    if (!_usernameValidator) {
      // handle to encode image to base 64
      String base64Image = base64Encode(_image.readAsBytesSync());

      UserModel user = UserModel(
          email: widget.newUser.email,
          fullname: widget.newUser.fullname,
          username: textUsername.text,
          password: widget.newUser.password,
          imgUrl: base64Image);
      BlocProvider.of<AuthenticationBloc>(context)
          .add(AuthEventRegisterForm(user: user));
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  void _onDontShowAgainChanged(bool newValue) {
    setState(() {
      dontShowAgain = newValue;

      if (dontShowAgain) {
        print("I wont show again. Sorry to bother you.");
      } else {
        print("We will meet again");
      }
    });
  }
}
