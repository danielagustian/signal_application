import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:signal_app/bloc/trending/trending-bloc.dart';
import 'package:signal_app/bloc/trending/trending-event.dart';
import 'package:signal_app/bloc/trending/trending-state.dart';
import 'package:signal_app/bloc/user/user-bloc.dart';
import 'package:signal_app/bloc/user/user-event.dart';
import 'package:signal_app/bloc/user/user-state.dart';
import 'package:signal_app/component/home-item.dart';
import 'package:signal_app/component/strategies/strategies-detail-item.dart';
import 'package:signal_app/component/user-item.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/models/trending-signal-model.dart';
import 'package:signal_app/models/user-model.dart';
import 'package:signal_app/screen/home/detail-item-screen.dart';
import 'package:signal_app/screen/profile/other-user-profile-screen.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen>
    with TickerProviderStateMixin {
  // Tab controller
  TabController _tabController;
  DateFormat formatter = DateFormat.yMMMMd();

  // Text controller
  TextEditingController textSearch = TextEditingController();

  // Selected index
  int _selectedIdx = 0;

  // Handle variable for trending signal
  List<TrendingSignalModel> trendings;

  // Handle variable for user list
  // List<UserModel> users;

  // handle search
  String query = "";

  void onChangeTab() {
    setState(() {
      _selectedIdx = _tabController.index;
    });
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);

    // handle on change tab
    _tabController.addListener(onChangeTab);

    // Load all trending signal
    BlocProvider.of<TrendingBloc>(context).add(TrendingEventLoadData());
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // Load all user
    BlocProvider.of<UserBloc>(context).add(UserEventLoadData());

    return WillPopScope(
      onWillPop: () {
        BlocProvider.of<TrendingBloc>(context).add(TrendingEventLoadData());
        return Future.value(true);
      },
      child: Scaffold(
        backgroundColor: colorBackground,
        body: SafeArea(
          child: NestedScrollView(
            headerSliverBuilder: (context, innerBoxIsScrolled) => [
              SliverToBoxAdapter(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () {
                              BlocProvider.of<TrendingBloc>(context)
                                  .add(TrendingEventLoadData());
                              Navigator.pop(context);
                            },
                            child: FaIcon(
                              FontAwesomeIcons.chevronLeft,
                              size: 20,
                              color: colorPrimary,
                            ),
                          ),
                          Container(
                            height: 80,
                            width: size.height <= 600
                                ? size.width * 0.8
                                : size.width * 0.84,
                            padding: EdgeInsets.symmetric(vertical: 20),
                            child: TextFormField(
                              controller: textSearch,
                              onChanged: (String val) {
                                // onSearch(val);
                                setState(() {
                                  query = val;
                                });
                                // print(query);
                              },
                              style: TextStyle(fontFamily: "Open-Sans"),
                              decoration: InputDecoration(
                                  hintText: "Search",
                                  hintStyle: TextStyle(color: colorPrimary30),
                                  contentPadding: EdgeInsets.only(top: 6),
                                  prefixIcon: Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 6, horizontal: 10),
                                    child: FaIcon(
                                      FontAwesomeIcons.search,
                                      color: colorPrimary30,
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(
                                        color: colorPrimary.withOpacity(0.3),
                                        width: 1.5),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      borderSide: BorderSide(
                                        color: colorPrimary30,
                                        width: 1.5,
                                      ))),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5),
                    Container(
                      height: 30,
                      decoration: BoxDecoration(
                          color: colorNeutral50,
                          borderRadius: BorderRadius.circular(5)),
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: TabBar(
                        controller: _tabController,
                        labelColor: colorBackground,
                        labelStyle: TextStyle(fontWeight: FontWeight.w700),
                        unselectedLabelColor: colorPrimary,
                        indicator: BoxDecoration(
                            color: colorPrimary,
                            borderRadius: BorderRadius.circular(5)),
                        tabs: [
                          Tab(
                            text: "Idea",
                          ),
                          Tab(
                            text: "Strategy",
                          ),
                          Tab(
                            text: "Profile",
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
            body: TabBarView(
              controller: _tabController,
              children: [
                BlocBuilder<TrendingBloc, TrendingState>(
                  builder: (context, state) {
                    if (state is TrendingStateSuccessLoad) {
                      return _selectedIdx == 0
                          ? state.trendings == null || query == ""
                              ? Container()
                              : buildIdeaBody(size, trendings: state.trendings)
                          : Container();
                    }

                    return Container();
                  },
                ),
                // Strategies signal showing
                Container(
                  child: ListView.builder(
                      padding: EdgeInsets.only(top: 10),
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      itemCount: 5,
                      itemBuilder: (context, index) => StrategiesDetailItem(
                            size: size,
                            title: "B${index}9Xb$index",
                            time: "Open time: 2020.02.21 07:34:49",
                            tp: "19.565",
                            sl: "19.265",
                          )),
                ),
                // User showing
                BlocBuilder<UserBloc, UserState>(
                  builder: (context, state) {
                    if (state is UserStateSuccessLoad) {
                      return _selectedIdx == 2
                          ? state.users == null || query == ""
                              ? Container()
                              : buildUserBody(size, users: state.users)
                          : Container();
                    }

                    return Container();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // Function to build user body
  Widget buildUserBody(Size size, {List<UserModel> users}) {
    return Container(
      child: ListView.builder(
          padding: EdgeInsets.only(top: 10),
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemCount: users.length,
          itemBuilder: (context, index) {
            if (query != null && query != "") {
              // show user that full name or username contains the query value
              if (users[index].username != null &&
                  (users[index]
                          .fullname
                          .toLowerCase()
                          .contains(query.toLowerCase()) ||
                      users[index]
                          .username
                          .toLowerCase()
                          .contains(query.toLowerCase()))) {
                return InkWell(
                  onTap: () {
                    // call to fetch the user data
                    BlocProvider.of<UserBloc>(context).add(
                        UserProfileEventLoadData(userID: users[index].userID));
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OtheUserScreen(
                                  id: users[index].userID,
                                  username: users[index].username,
                                )));
                  },
                  child: UserItem(
                    fullname: users[index].fullname,
                    username: users[index].username,
                    imgURL: users[index].imgUrl,
                    size: size,
                  ),
                );
              }
            }

            return Container();
            // );
          }),
    );
  }

  // Widget to build Idea Body
  Widget buildIdeaBody(Size size, {List<TrendingSignalModel> trendings}) {
    return Container(
      child: ListView.builder(
          padding: EdgeInsets.only(top: 10),
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemCount: trendings.length,
          itemBuilder: (context, index) {
            String action = "SELL";
            if (trendings[index].action == "OP_BUY") action = "BUY";

            // for searching
            if (trendings[index]
                    .judul
                    .toLowerCase()
                    .contains(query.toLowerCase()) ||
                trendings[index]
                    .action
                    .toLowerCase()
                    .contains(query.toLowerCase()) ||
                trendings[index]
                    .namaUser
                    .toLowerCase()
                    .contains(query.toLowerCase())) {
              return InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              DetailItem(id: trendings[index].id)));
                },
                child: CardItem(
                  size: size,
                  title: "${trendings[index].judul}",
                  badge: "${trendings[index].pair.toUpperCase()} $action",
                  date: "${formatter.format(trendings[index].date)}",
                  user: "${trendings[index].namaUser}",
                  reads: "${trendings[index].readTime} mins",
                ),
              );
            }

            return Container();
          }),
    );
  }
}
