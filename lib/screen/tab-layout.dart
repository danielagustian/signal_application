import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/screen/home/home-screen.dart';
import 'package:signal_app/screen/post-signal/post-signal-screen.dart';
import 'package:signal_app/screen/profile/profile-screen.dart';
import 'package:signal_app/screen/strategies/strategies-screen.dart';

class MainTabLayout extends StatefulWidget {
  @override
  _MainTabLayoutState createState() => _MainTabLayoutState();
}

class _MainTabLayoutState extends State<MainTabLayout> {
  List<Widget> pageList = [HomeScreen(), PostSignalScreen(), ProfileScreen()];
  int pageIndex = 0;

  void onSelectPage(int index) {
    setState(() {
      pageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pageList[pageIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color(0xFFF9FBFB),
        currentIndex: pageIndex,
        onTap: (value) {
          onSelectPage(value);
        },
        selectedItemColor: colorPrimary,
        selectedLabelStyle: TextStyle(color: colorPrimary),
        unselectedItemColor: colorPrimary30,
        unselectedLabelStyle: TextStyle(color: colorPrimary30),
        items: [
          BottomNavigationBarItem(
              icon: FaIcon(
                FontAwesomeIcons.home,
              ),
              label: "Home"),
          BottomNavigationBarItem(
              icon: FaIcon(
                FontAwesomeIcons.plusCircle,
              ),
              label: "Post"),
          BottomNavigationBarItem(
              icon: FaIcon(
                FontAwesomeIcons.userCircle,
              ),
              label: "Profile"),
        ],
      ),
    );
  }
}
