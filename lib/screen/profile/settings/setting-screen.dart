import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signal_app/component/profile/setting-item.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/screen/authentication/login-screen.dart';
import 'package:signal_app/screen/profile/settings/profile-settings-screen.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: colorBackground,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: colorBackground,
        title: Text(
          "Settings",
          style: TextStyle(
              color: colorSemanticBlack,
              fontWeight: FontWeight.w700,
              fontSize: size.height <= 600 ? 18 : 22),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: FaIcon(
            FontAwesomeIcons.chevronLeft,
            color: colorPrimary,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
            padding: size.width < 500
                ? EdgeInsets.all(20)
                : EdgeInsets.symmetric(vertical: 20, horizontal: 40),
            child: Column(
              children: [
                SettingItem(
                  size: size,
                  onClick: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProfileSettingScreen()));
                  },
                  widget: CircleAvatar(
                    radius: 10,
                    backgroundColor: colorDarkNeutral,
                  ),
                  title: "Profile",
                ),
                SettingItem(
                  size: size,
                  onClick: () {},
                  widget: CircleAvatar(
                    radius: 10,
                    backgroundColor: colorDarkNeutral,
                  ),
                  title: "Account",
                ),
                SettingItem(
                  size: size,
                  onClick: () {},
                  widget: CircleAvatar(
                    radius: 10,
                    backgroundColor: colorDarkNeutral,
                  ),
                  title: "Notifications",
                ),
                SettingItem(
                  size: size,
                  onClick: () {},
                  widget: CircleAvatar(
                    radius: 10,
                    backgroundColor: colorDarkNeutral,
                  ),
                  title: "Privacy",
                ),
                SettingItem(
                  size: size,
                  onClick: () {},
                  widget: CircleAvatar(
                    radius: 10,
                    backgroundColor: colorDarkNeutral,
                  ),
                  title: "Help",
                ),
                SettingItem(
                  size: size,
                  onClick: () {},
                  widget: CircleAvatar(
                    radius: 10,
                    backgroundColor: colorDarkNeutral,
                  ),
                  title: "About",
                ),
                SettingItem(
                  size: size,
                  onClick: onLogout,
                  widget: CircleAvatar(
                    radius: 10,
                    backgroundColor: colorDarkNeutral,
                  ),
                  title: "Log out",
                ),
              ],
            )),
      ),
    );
  }

  void onLogout() async {
    // Save token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("token");
    prefs.remove("userID");

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoginScreen()));
  }
}
