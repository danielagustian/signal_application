import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:signal_app/component/long-button.dart';
import 'package:signal_app/constant/constant.dart';

class ProfileSettingScreen extends StatefulWidget {
  @override
  _ProfileSettingScreenState createState() => _ProfileSettingScreenState();
}

class _ProfileSettingScreenState extends State<ProfileSettingScreen> {
  // Text Controller
  TextEditingController textNama =
      TextEditingController(text: "Finley Khouwira");
  TextEditingController textUsername =
      TextEditingController(text: "dijawabsaja");
  TextEditingController textEmail =
      TextEditingController(text: "finley@yokesen.com");

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: colorBackground,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: colorBackground,
        title: Text(
          "Profile",
          style: TextStyle(
              color: colorSemanticBlack,
              fontWeight: FontWeight.w600,
              fontSize: size.height <= 600 ? 20 : 25),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: FaIcon(
            FontAwesomeIcons.chevronLeft,
            color: colorPrimary,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        padding: size.width < 500
            ? EdgeInsets.all(20)
            : EdgeInsets.symmetric(vertical: 20, horizontal: 40),
        child: ListView(
          scrollDirection: Axis.vertical,
          children: [
            CircleAvatar(
              backgroundColor: colorNeutral70,
              radius: 70,
              child: Center(
                child: SvgPicture.asset(
                  "assets/icons/camera.svg",
                  color: colorDarkNeutral,
                ),
              ),
            ),
            SizedBox(height: 10),
            Align(
              alignment: Alignment.topCenter,
              child: InkWell(
                  onTap: () {},
                  child: Text(
                    "Click to change",
                    style: TextStyle(
                        color: colorPrimary,
                        fontSize: size.height <= 600 ? 12 : 14),
                  )),
            ),
            SizedBox(height: 20),
            Container(
                child: Stack(
              alignment: Alignment.centerLeft,
              children: [
                Builder(builder: (context) {
                  return Text(
                    'Name',
                    style: TextStyle(
                        color: colorPrimary,
                        fontWeight: FontWeight.bold,
                        fontSize: size.height <= 600 ? 12 : 14),
                  );
                }),
                TextFormField(
                  controller: textNama,
                  textAlign: TextAlign.right,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 50, right: 10),
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none),
                ),
              ],
            )),
            Container(
                child: Stack(
              alignment: Alignment.centerLeft,
              children: [
                Builder(builder: (context) {
                  return Text(
                    'Username',
                    style: TextStyle(
                        color: colorPrimary,
                        fontWeight: FontWeight.bold,
                        fontSize: size.height <= 600 ? 12 : 14),
                  );
                }),
                TextFormField(
                  controller: textUsername,
                  textAlign: TextAlign.right,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 50, right: 10),
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none),
                ),
              ],
            )),
            Container(
                child: Stack(
              alignment: Alignment.centerLeft,
              children: [
                Builder(builder: (context) {
                  return Text(
                    'Email',
                    style: TextStyle(
                        color: colorPrimary,
                        fontWeight: FontWeight.bold,
                        fontSize: size.height <= 600 ? 12 : 14),
                  );
                }),
                TextFormField(
                  controller: textEmail,
                  textAlign: TextAlign.right,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 50, right: 10),
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none),
                ),
              ],
            )),
            Container(
              margin: EdgeInsets.only(top: 16),
              padding: EdgeInsets.only(right: 14),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Connect MT4",
                      style: TextStyle(
                          color: colorPrimary,
                          fontWeight: FontWeight.bold,
                          fontSize: size.height <= 600 ? 12 : 14)),
                  Text("Connected",
                      style: TextStyle(
                          color: colorSemanticBlack,
                          fontWeight: FontWeight.w500,
                          fontSize: size.height <= 600 ? 14 : 16)),
                ],
              ),
            ),
            SizedBox(
                height: size.height <= 600
                    ? size.height * 0.09
                    : size.height * 0.28),
            LongButton(
              size: size,
              title: "Update",
              textColor: colorBackground,
              bgColor: colorPrimary,
              onClick: () {},
            )
          ],
        ),
      ),
    );
  }
}
