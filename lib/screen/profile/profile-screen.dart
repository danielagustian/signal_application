import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signal_app/bloc/user/user-bloc.dart';
import 'package:signal_app/bloc/user/user-event.dart';
import 'package:signal_app/bloc/user/user-state.dart';
import 'package:signal_app/component/home-item.dart';
import 'package:signal_app/component/strategies-item.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/screen/profile/settings/setting-screen.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  int userID;

  // Get user id
  void loadUserProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      userID = prefs.getInt("userID");
    });

    BlocProvider.of<UserBloc>(context)
        .add(UserProfileEventLoadData(userID: userID));
  }

  @override
  void initState() {
    super.initState();
    loadUserProfile();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: colorBackground,
      body: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          if (state is UserProfileStateSuccessLoad) {
            return buildMainBody(size, state, context);
          } else if (state is UserStateFailLoad) {
            Container(child: Center(child: Text("No data found")));
          }
          return Container(
            width: size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  backgroundColor: colorBackground,
                  valueColor: AlwaysStoppedAnimation(colorPrimary),
                ),
                SizedBox(height: 10),
                Text("Fetching latest data",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15))
              ],
            ),
          );
        },
      ),
    );
  }

  // Widget to show the main body
  Widget buildMainBody(
      Size size, UserProfileStateSuccessLoad state, BuildContext context) {
    return Container(
      margin: size.width < 500
          ? EdgeInsets.zero
          : EdgeInsets.symmetric(horizontal: 30),
      child: ListView(
        primary: true,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Row(
              children: [
                CircleAvatar(
                  backgroundColor: colorDarkNeutral,
                  radius: 30,
                  child: (state.user.imgUrl == null || state.user.imgUrl == "")
                      ? Container()
                      : Image.network("${state.user.imgUrl}"),
                ),
                SizedBox(width: 10),
                Flexible(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("${state.user.fullname}",
                              style: TextStyle(
                                  color: colorSemanticBlack,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700)),
                          SizedBox(height: 5),
                          Text("${state.user.username}",
                              style: TextStyle(
                                  color: colorSemanticBlack, fontSize: 12)),
                        ],
                      ),
                      Container(
                        child: Row(
                          children: [
                            InkWell(
                                onTap: () {},
                                child:
                                    SvgPicture.asset("assets/icons/share.svg")),
                            SizedBox(width: 20),
                            InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => SettingScreen(),
                                      ));
                                },
                                child: SvgPicture.asset(
                                    "assets/icons/setting.svg")),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 5),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text("${state.user.bio}",
                textAlign: TextAlign.justify,
                style: TextStyle(
                  height: 1,
                  color: colorSemanticBlack,
                  fontSize: 12,
                )),
          ),
          SizedBox(
              height: state.user.bio == null || state.user.bio == "" ? 10 : 20),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            width: size.width,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ColumnTextBlacPrimary(
                  size: size,
                  text1: "Followers",
                  text2: "${state.user.followerCount}",
                ),
                ColumnTextBlacPrimary(
                  size: size,
                  text1: "Following",
                  text2: "${state.user.followingCount}",
                ),
                ColumnTextBlacPrimary(
                  size: size,
                  text1: "Posting",
                  text2: "${state.user.postingCount}",
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          signalItemBuilder(size)
        ],
      ),
    );
  }

  // Widget to build signal list item
  Widget signalItemBuilder(Size size) {
    return Container(
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: 10,
        itemBuilder: (context, index) => CardItem(
          size: size,
          title: "Title $index aa aaaaaa aaaaa aaaaaaaaaa aaaaaaaaaaa",
          badge: "PAIR",
          date: "$index Maret 2021",
          user: "user $index",
          reads: "$index mins",
        ),
      ),
    );
  }
}
