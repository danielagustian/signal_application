import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:signal_app/bloc/user/user-bloc.dart';
import 'package:signal_app/bloc/user/user-state.dart';
import 'package:signal_app/component/home-item.dart';
import 'package:signal_app/component/strategies-item.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/models/user-model.dart';

class OtheUserScreen extends StatefulWidget {
  const OtheUserScreen({Key key, this.id, this.username}) : super(key: key);

  @override
  _OtheUserScreenState createState() => _OtheUserScreenState();

  final int id;
  final String username;
}

class _OtheUserScreenState extends State<OtheUserScreen> {
  // Buat list filter dan sort
  List<String> filterItems = ["filter1", "filter2", "filter3"];
  List<String> sortList = ["sort1", "sort2", "sort3"];
  String choosedSort = "sort1";
  String choosedFilter;

  UserModel user;

  @override
  void initState() {
    super.initState();
    choosedFilter = filterItems[0];
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: colorBackground,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: colorBackground,
        title: Text(
          "${widget.username}",
          style: TextStyle(
              color: colorSemanticBlack,
              fontWeight: FontWeight.w700,
              fontSize: size.height <= 600 ? 18 : 22),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: FaIcon(
            FontAwesomeIcons.chevronLeft,
            color: colorPrimary,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            icon: FaIcon(
              FontAwesomeIcons.ellipsisH,
              color: colorPrimary,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
      body: BlocListener<UserBloc, UserState>(
        listener: (context, state) {
          if (state is UserProfileStateSuccessLoad) {
            setState(() {
              user = state.user;
            });
          }
        },
        child: user == null
            ? Container(
                width: size.width,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: colorBackground,
                    valueColor: AlwaysStoppedAnimation(colorPrimary),
                  ),
                ),
              )
            : buildMainBody(size),
      ),
    );
  }

  // function to show main body
  Widget buildMainBody(Size size) {
    return Container(
      margin: size.width < 500
          ? EdgeInsets.zero
          : EdgeInsets.symmetric(horizontal: 30),
      child: ListView(
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 5, left: 20, right: 20),
            decoration: BoxDecoration(color: colorBackground, boxShadow: [
              BoxShadow(
                  offset: Offset(0, 0),
                  color: colorPrimary.withOpacity(0.1),
                  blurRadius: 10)
            ]),
            child: Column(
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                        height: size.height <= 600 ? 80 : 90,
                        width: size.height <= 600 ? 80 : 90,
                        decoration: BoxDecoration(
                            color: colorDarkNeutral,
                            borderRadius: BorderRadius.circular(100)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: user.imgUrl == "" || user.imgUrl == null
                              ? Container()
                              : Image.network(user.imgUrl),
                        ),
                      ),
                      SizedBox(width: 10),
                      userProfile(size,
                          fullname: "${user.fullname}",
                          follower: user.followerCount,
                          following: user.followingCount,
                          post: user.postingCount)
                    ],
                  ),
                ),
                Text("${user.bio}",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      height: 1,
                      color: colorSemanticBlack,
                      fontSize: 12,
                    )),
                SizedBox(height: 10),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ElevatedButton(
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(8.0),
                          primary: colorPrimary,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          textStyle: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w500),
                        ),
                        child: Container(
                          width: size.height <= 600
                              ? size.width * 0.7
                              : size.width * 0.75,
                          height: size.height * 0.03,
                          child: Center(child: Text("Follow")),
                        ),
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 6, horizontal: 8),
                        decoration: BoxDecoration(
                            border: Border.all(color: colorPrimary, width: 1),
                            borderRadius: BorderRadius.circular(8)),
                        child: InkWell(
                            child: FaIcon(
                              FontAwesomeIcons.chevronDown,
                              color: colorPrimary,
                              size: 20,
                            ),
                            onTap: () {}),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                DropdownButton<String>(
                    onChanged: (val) {
                      setState(() {
                        choosedFilter = val;
                      });
                    },
                    value: choosedFilter,
                    underline: Container(),
                    items: filterItems
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                          value: value, child: Text(value));
                    }).toList()),
                SizedBox(width: 10),
                DropdownButton<String>(
                    onChanged: (val) {
                      setState(() {
                        choosedFilter = val;
                      });
                    },
                    value: choosedSort,
                    underline: Container(),
                    items:
                        sortList.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                          value: value, child: Text(value));
                    }).toList())
              ],
            ),
          ),
          ...filterItems.map((e) => CardItem(
                size: size,
                badge: "PAIR",
                date: "2020-09-11",
                reads: "3 mins",
                title: "Lorem Ipsum dolor sit",
                user: "user x $e",
              ))
        ],
      ),
    );
  }

  // Widget to show profile data other user
  Widget userProfile(Size size,
      {String fullname, int follower, int following, int post}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "$fullname",
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: colorSemanticBlack,
                fontWeight: FontWeight.w700,
                fontSize: size.height <= 600 ? 14 : 16),
          ),
          SizedBox(height: 10),
          Container(
            width: size.height <= 600 ? size.width * 0.5 : size.width * 0.6,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ColumnTextBlacPrimary(
                  size: size,
                  text1: "Follower",
                  text2: "$follower",
                ),
                ColumnTextBlacPrimary(
                  size: size,
                  text1: "Following",
                  text2: "$following",
                ),
                ColumnTextBlacPrimary(
                  size: size,
                  text1: "Posting",
                  text2: "$post",
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
