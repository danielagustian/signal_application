import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:signal_app/component/home-item.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/screen/search-screen.dart';
import 'package:signal_app/screen/strategies/strategies-detail-screen.dart';

class StrategiesScreen extends StatefulWidget {
  @override
  _StrategiesScreenState createState() => _StrategiesScreenState();
}

class _StrategiesScreenState extends State<StrategiesScreen>
    with TickerProviderStateMixin {
  // For Filter
  String choosedPairFilter = "A921MD0";

  // Draggable scroll controller
  AnimationController _controller;
  Duration _duration = Duration(milliseconds: 600);
  Tween<Offset> _tween = Tween(begin: Offset(0, 1), end: Offset(0, 0));

  @override
  void initState() {
    super.initState();

    // Init for dragabble controller
    _controller = AnimationController(vsync: this, duration: _duration);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: colorBackground,
        elevation: 0,
        title: Text(
          "Strategies",
          style: TextStyle(
              color: Colors.black,
              fontSize: size.height < 600 ? 22 : 25,
              fontWeight: FontWeight.w700),
        ),
        actions: [
          IconButton(
              icon: FaIcon(
                FontAwesomeIcons.filter,
                color: colorPrimary,
              ),
              onPressed: () {
                if (_controller.isDismissed) {
                  _controller.forward();
                } else if (_controller.isCompleted) {
                  _controller.reverse();
                }
              }),
          IconButton(
              icon: FaIcon(
                FontAwesomeIcons.search,
                color: colorPrimary,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchScreen()));
              })
        ],
      ),
      backgroundColor: colorBackground,
      body: Stack(
        children: [
          Container(
            margin: size.width < 500
                ? EdgeInsets.zero
                : EdgeInsets.symmetric(horizontal: 30),
            child: ListView.builder(
                itemCount: 8,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => StrategiesDetailScreen()));
                    },
                    child: CardItem(
                      size: size,
                      title: "Title $index asfkj 3paj assad",
                      badge: "new",
                      date: "$index Maret 2021",
                      user: "user $index",
                      reads: "$index mins",
                    ),
                  );
                }),
          ),
          scrollBottomSheet(size)
        ],
      ),
    );
  }

  // Show scrollable sheet
  Widget scrollBottomSheet(Size size) {
    return Container(
      child: SlideTransition(
        position: _tween.animate(_controller),
        child: DraggableScrollableSheet(
          initialChildSize: 0.5,
          maxChildSize: 0.9,
          minChildSize: 0.2,
          builder: (context, scrollController) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              decoration: BoxDecoration(
                  color: colorBackground,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 20,
                        offset: Offset(0, -4),
                        color: Color(0xFF505050).withOpacity(0.5))
                  ]),
              child: ListView(
                controller: scrollController,
                children: [
                  Align(
                    child: Container(
                        height: 5,
                        width: 50,
                        decoration: BoxDecoration(
                          color: colorDarkNeutral,
                          borderRadius: BorderRadius.circular(20),
                        )),
                  ),
                  SizedBox(height: 40),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text("Filter",
                            style: TextStyle(
                                color: colorSemanticBlack,
                                fontWeight: FontWeight.bold,
                                fontSize: 20)),
                        Text("Confirm",
                            style: TextStyle(
                                color: colorPrimary,
                                fontWeight: FontWeight.bold,
                                fontSize: size.height <= 600 ? 14 : 16)),
                      ],
                    ),
                  ),
                  SizedBox(height: 30),
                  Container(
                    child: Column(
                      children: [
                        filterTitle(size, title: "By Pair", onClick: () {}),
                        SizedBox(height: 14),
                        Container(
                            width: size.width,
                            height: size.width < 500 ? 70 : 100,
                            child: GridView.builder(
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      mainAxisSpacing: 5,
                                      crossAxisCount: 4,
                                      crossAxisSpacing: 10,
                                      childAspectRatio: 5.5 / 2),
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 8,
                              itemBuilder: (context, index) => InkWell(
                                  onTap: () {
                                    setState(() {
                                      choosedPairFilter = "A921MD$index";
                                    });
                                  },
                                  child: filterItem(title: "A921MD$index")),
                            ))
                      ],
                    ),
                  ),
                  SizedBox(height: 30),
                  Container(
                    child: Column(
                      children: [
                        filterTitle(size,
                            title: "By Performance", onClick: () {}),
                        SizedBox(height: 14),
                        Container(
                            width: size.width,
                            height: size.width < 500 ? 70 : 100,
                            child: GridView.builder(
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      mainAxisSpacing: 5,
                                      crossAxisCount: 4,
                                      crossAxisSpacing: 10,
                                      childAspectRatio: 5.5 / 2),
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 8,
                              itemBuilder: (context, index) => InkWell(
                                  onTap: () {
                                    print("jalan");
                                    setState(() {
                                      choosedPairFilter = "A921MD$index";
                                    });
                                  },
                                  child: filterItem(title: "A921MD$index")),
                            ))
                      ],
                    ),
                  ),
                  SizedBox(height: 30),
                  Container(
                    child: Column(
                      children: [
                        filterTitle(size, title: "By Anything", onClick: () {}),
                        SizedBox(height: 14),
                        Container(
                            width: size.width,
                            height: size.width < 500 ? 70 : 100,
                            child: GridView.builder(
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      mainAxisSpacing: 5,
                                      crossAxisCount: 4,
                                      crossAxisSpacing: 10,
                                      childAspectRatio: 5.5 / 2),
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 8,
                              itemBuilder: (context, index) => InkWell(
                                  onTap: () {
                                    setState(() {
                                      choosedPairFilter = "A921MD$index";
                                    });
                                  },
                                  child: filterItem(title: "A921MD$index")),
                            ))
                      ],
                    ),
                  ),
                  SizedBox(height: 30),
                  Align(
                    alignment: Alignment.topCenter,
                    child: IconButton(
                      onPressed: () => _controller.reverse(),
                      icon: FaIcon(
                        FontAwesomeIcons.timesCircle,
                        size: 40,
                        color: colorError,
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  // Filter title
  Widget filterItem({String title}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
      decoration: title == choosedPairFilter
          ? BoxDecoration(
              color: colorPrimary, borderRadius: BorderRadius.circular(6))
          : BoxDecoration(
              color: colorBackground,
              border: Border.all(color: colorNeutral, width: 1),
              borderRadius: BorderRadius.circular(6)),
      child: Center(
        child: Text(
          "$title",
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: title == choosedPairFilter
                  ? colorBackground
                  : colorDarkNeutral,
              fontWeight: FontWeight.w500,
              fontSize: 12),
        ),
      ),
    );
  }

  // Filter item title
  Widget filterTitle(Size size, {String title, Function onClick}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text("$title",
            style: TextStyle(
                color: colorSemanticBlack,
                fontWeight: FontWeight.w600,
                fontSize: 16)),
        InkWell(
          onTap: onClick,
          child: Text("Show All",
              style: TextStyle(
                  color: colorPrimary, fontSize: size.height <= 600 ? 12 : 14)),
        ),
      ],
    );
  }
}
