import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:signal_app/component/strategies/modal-bottom-item.dart';
import 'package:signal_app/component/strategies/strategies-detail-item.dart';
import 'package:signal_app/component/strategies-item.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/screen/profile/other-user-profile-screen.dart';

class StrategiesDetailScreen extends StatefulWidget {
  @override
  _StrategiesDetailScreenState createState() => _StrategiesDetailScreenState();
}

class _StrategiesDetailScreenState extends State<StrategiesDetailScreen>
    with TickerProviderStateMixin {
  // COntroller
  ScrollController parent1 = ScrollController();
  TabController _tabController;

  // scroll controller function for handle scroll in parent and child
  scrollHandler() {}

  @override
  void initState() {
    super.initState();

    // Listener Scroll controller
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: colorBackground,
      body: NestedScrollView(
        headerSliverBuilder: (context, innerBoxIsScrolled) => [
          SliverAppBar(
            pinned: false,
            backgroundColor: colorBackground,
            title: Text(
              "Strategies Name",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: size.height < 600 ? 20 : 25),
            ),
            centerTitle: true,
            leading: IconButton(
              icon: FaIcon(
                FontAwesomeIcons.chevronLeft,
                color: colorPrimary,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
          SliverToBoxAdapter(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: size.width < 500 ? 20 : 40),
                  child: Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa quisque feugiat sit sagittis hendrerit dignissim. Bibendum senectus aliquet donec facilisi a purus egestas. Mollis morbi id non egestas lectus suspendisse. Est rhoncus, molestie viverra lacus diam lorem velit. Sed neque leo netus lectus sed sit.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontSize: size.height < 600 ? 14 : 16),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: size.width < 500 ? 20 : 40),
                  width: size.width,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Posted By",
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            color: colorDarkNeutral,
                            fontWeight: FontWeight.w500,
                            fontSize: 12),
                      ),
                      SizedBox(width: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OtheUserScreen()));
                        },
                        child: Row(
                          children: [
                            CircleAvatar(
                              backgroundColor: colorClear,
                              radius: size.width < 400 ? 16 : 18,
                            ),
                            SizedBox(width: 10),
                            Text(
                              "User",
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w500,
                                  fontSize: size.width < 400 ? 16 : 18),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30),
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: size.width < 500 ? 20 : 40),
                  width: size.width,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ColumnTextBlacPrimary(
                        size: size,
                        text1: "Gain",
                        text2: "892.4%",
                      ),
                      ColumnTextBlacPrimary(
                        size: size,
                        text1: "Joined Member",
                        text2: "2731",
                      ),
                      ColumnTextBlacPrimary(
                        size: size,
                        text1: "Trades",
                        text2: "22",
                      ),
                      ColumnTextBlacPrimary(
                        size: size,
                        text1: "Duration",
                        text2: "67d",
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30),
                Container(
                  height: 30,
                  decoration: BoxDecoration(
                      color: colorNeutral50,
                      borderRadius: BorderRadius.circular(5)),
                  margin: EdgeInsets.symmetric(
                      horizontal: size.width < 500 ? 20 : 40),
                  child: TabBar(
                    controller: _tabController,
                    labelColor: colorBackground,
                    labelStyle: TextStyle(fontWeight: FontWeight.w700),
                    unselectedLabelColor: colorPrimary,
                    indicator: BoxDecoration(
                        color: colorPrimary,
                        borderRadius: BorderRadius.circular(5)),
                    tabs: [
                      Tab(
                        text: "Open",
                      ),
                      Tab(
                        text: "Closed",
                      ),
                      Tab(
                        text: "Pre-Open",
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
        body: TabBarView(
          controller: _tabController,
          children: [
            Container(
              margin: size.width < 500
                  ? EdgeInsets.zero
                  : EdgeInsets.symmetric(horizontal: 30),
              child: ListView.builder(
                padding: EdgeInsets.only(top: 10),
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemCount: 25,
                itemBuilder: (context, index) => InkWell(
                    onTap: () {
                      modalBottomSheet(context);
                    },
                    child: StrategiesDetailItem(
                      size: size,
                      title: "B${index}9Xb$index",
                      time: "Open time: 2020.02.21 07:34:49",
                      tp: "19.565",
                      sl: "19.265",
                    )),
              ),
            ),
            Center(
              child: Text("Closed"),
            ),
            Center(
              child: Text("Pre Open"),
            ),
          ],
        ),
      ),
    );
  }

  // Show Modal bottom sheet
  void modalBottomSheet(BuildContext context) {
    Scaffold.of(context).showBottomSheet<void>(
      (BuildContext context) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          decoration: BoxDecoration(
              color: colorBackground,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20), topLeft: Radius.circular(20)),
              boxShadow: [
                BoxShadow(
                    blurRadius: 20,
                    offset: Offset(0, -4),
                    color: Color(0xFF505050).withOpacity(0.5))
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                  height: 5,
                  width: 50,
                  decoration: BoxDecoration(
                    color: colorDarkNeutral,
                    borderRadius: BorderRadius.circular(20),
                  )),
              SizedBox(height: 40),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RichText(
                        text: TextSpan(children: [
                      TextSpan(
                          text: "SELL",
                          style: TextStyle(
                              color: colorSemanticRed,
                              fontWeight: FontWeight.bold,
                              fontSize: 24)),
                      TextSpan(
                          text: " XA4H11",
                          style: TextStyle(
                              color: colorSemanticBlack,
                              fontWeight: FontWeight.bold,
                              fontSize: 24)),
                    ])),
                    Text("-7.25%",
                        style: TextStyle(
                            color: colorSemanticRed,
                            fontWeight: FontWeight.bold,
                            fontSize: 24)),
                  ],
                ),
              ),
              SizedBox(height: 16),
              ModalBottomSheetItem(
                text1: "ID",
                text2: "bb23r88120ue",
              ),
              SizedBox(height: 10),
              ModalBottomSheetItem(
                text1: "Open Time",
                text2: "2019.12.17 08:40:47",
              ),
              SizedBox(height: 10),
              ModalBottomSheetItem(
                text1: "Open Price",
                text2: "1.53324",
              ),
              SizedBox(height: 10),
              ModalBottomSheetItem(
                text1: "S/L",
                text2: "1.12341",
              ),
              SizedBox(height: 10),
              ModalBottomSheetItem(
                text1: "T/P",
                text2: "1.33421",
              ),
              SizedBox(height: 10),
              ModalBottomSheetItem(
                text1: "Lots",
                text2: "100.00",
              ),
              SizedBox(height: 10),
              ModalBottomSheetItem(
                text1: "Close Time",
                text2: "2019.12.17 08:40:47",
              ),
              SizedBox(height: 25),
              ModalBottomSheetItem(
                text1: "Close Price",
                text2: "1.112144",
              ),
              SizedBox(height: 10),
              ModalBottomSheetItem(
                text1: "Pips",
                text2: "-15.6",
              ),
              SizedBox(height: 10),
              ModalBottomSheetItem(
                text1: "Duration",
                text2: "00h01m",
              ),
              SizedBox(height: 10),
              ModalBottomSheetItem(
                text1: "Profit",
                text2: "-15600",
              ),
              SizedBox(height: 50),
            ],
          ),
        );
      },
    );
  }
}
