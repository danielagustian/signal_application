import 'dart:async';

import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signal_app/bloc/authentication/auth-bloc.dart';
import 'package:signal_app/bloc/comment/comment-bloc.dart';
import 'package:signal_app/bloc/trending/trending-bloc.dart';
import 'package:signal_app/bloc/trending/trending-detail/trending-detail-bloc.dart';
import 'package:signal_app/bloc/user/user-bloc.dart';
import 'package:signal_app/constant/constant.dart';
import 'package:signal_app/screen/authentication/login-screen.dart';
import 'package:signal_app/screen/onboarding/onboarding-screen.dart';
import 'package:signal_app/screen/tab-layout.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = BlocObserver();

  String token;
  int install = 0;
  bool remember = false;
  SharedPreferences prefs = await SharedPreferences.getInstance();

  // check, is the user want to keep session
  remember = prefs.getBool("remember");

  if (remember == null || !remember) {
    print(remember);
    await prefs.remove("token");
  }

  // check is user have been login
  token = prefs.getString("token");
  // check to show or not showing the onboarding screen
  install = prefs.getInt("install");

  runApp(DevicePreview(
    enabled: !kReleaseMode,
    builder: (context) => MyApp(
      token: token,
      install: install,
    ),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final String token;
  final int install;

  const MyApp({Key key, this.token, this.install}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticationBloc>(
          create: (context) => AuthenticationBloc(),
        ),
        BlocProvider<TrendingBloc>(
          create: (context) => TrendingBloc(),
        ),
        BlocProvider<CommentBloc>(
          create: (context) => CommentBloc(),
        ),
        BlocProvider<TrendingDetailBloc>(
          create: (context) => TrendingDetailBloc(),
        ),
        BlocProvider<UserBloc>(
          create: (context) => UserBloc(),
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(
          fontFamily: 'Nunito',
        ),
        locale: DevicePreview.locale(context), // Add the locale here
        builder: DevicePreview.appBuilder, // Add the builder here
        home: MyHomePage(
          token: token,
          install: install,
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key, this.token, this.install}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
  final String token;
  final int install;
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();

    // After 3 second, move into `login` or `home` screen
    Timer(
        Duration(seconds: 3),
        () => Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                // If user have install the app
                builder: (context) => widget.install != 1
                    ? OnBoardingScreen()
                    // if user has been login
                    : widget.token != null
                        ? MainTabLayout()
                        : LoginScreen())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorBackground,
      body: Center(
        child: Text(
          "Traderindo",
          style: TextStyle(
              color: colorPrimary, fontWeight: FontWeight.bold, fontSize: 30),
        ),
      ),
    );
  }
}
