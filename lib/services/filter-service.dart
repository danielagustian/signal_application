import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class FilterService {
  final baseURI = "api.traderindo.com";
  final fullBaseURI = "https://api.traderindo.com";

  // Get all category
  Future getFilterCategory({String category}) async {
    // Get user token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    // Merge url and params query into endpoint
    var paramsQuery = {'cat': category};
    Uri url = Uri.parse("$fullBaseURI/api/category")
        .replace(queryParameters: paramsQuery);

    print(url);

    // Get data
    var response = await http.get(url, headers: <String, String>{
      'Content-Type': 'application/json',
      'Charset': 'utf-8',
      'Authorization': 'Bearer $token'
    });
    print(response.statusCode.toString());

    if (response.statusCode >= 200 && response.statusCode < 300) {
      // then parse the JSON.
      // print("response.body:" + response.body);

      // decode the data
      var responseJson = jsonDecode(response.body);
      List<String> filterList = [];

      if (responseJson != null) {
        if (category == "action") {
          responseJson["action"].forEach((data) {
            filterList.add(data["action"]);
          });
        } else {
          responseJson.forEach((data) {
            filterList.add(data["pair"]);
          });
        }
      }

      return filterList;
    } else {
      return null;
    }
  }
}
