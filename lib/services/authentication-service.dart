import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:signal_app/models/auth-model.dart';
import 'package:signal_app/models/user-model.dart';

class AuthenticationService {
  final baseURI = "api.traderindo.com";
  final fullBaseURI = "https://api.traderindo.com";

  Future<AuthModel> login({String email, String password}) async {
    final response = await http.post(
      Uri.https(baseURI, '/api/login'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Charset': 'utf-8'
      },
      body: jsonEncode(<String, String>{'email': email, 'password': password}),
    );
    print(response.statusCode.toString());

    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      // print("response.body:" + response.body);

      final user = AuthModel.fromJson(jsonDecode(response.body));

      return user;
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      // throw Exception('Failed to login');
      return null;
    }
  }

  Future register({UserModel user}) async {
    final response = await http.post(
      Uri.https(baseURI, '/api/register'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Charset': 'utf-8'
      },
      body: jsonEncode(<String, String>{
        'email': user.email,
        'name': user.fullname,
        'username': user.username,
        'password': user.password,
        'password_confirmation': user.password,
        'image': user.imgUrl
      }),
    );
    print(response.statusCode.toString());

    if (response.statusCode >= 200 && response.statusCode < 300) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      // print("response.body:" + response.body);

      final user = AuthModel.fromJson(jsonDecode(response.body));

      return user;
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      // throw Exception('Failed to login');
      String res = "";
      if (response.body.contains("email")) {
        res = "email";
      } else {
        res = "username";
      }
      return res;
    }
  }
}
