import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signal_app/models/trending-signal-model.dart';

class TrendingService {
  final baseURI = "api.traderindo.com";
  final fullBaseURI = "https://api.traderindo.com";

  // Get all trending signal data
  Future<List<TrendingSignalModel>> getAllData({String query}) async {
    // Get user token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    // Merge url and query params into endpoint
    var paramsQuery = {'action': query};
    // query is by pair
    if (query != null && query != "" && !query.contains("op_")) {
      paramsQuery = {'pair': query};
    }
    Uri url = Uri.parse("$fullBaseURI/api/signals")
        .replace(queryParameters: paramsQuery);

    var response = await http.get(url, headers: <String, String>{
      'Content-Type': 'application/json',
      'Charset': 'utf-8',
      'Authorization': 'Bearer $token'
    });
    print(response.statusCode.toString());

    if (response.statusCode == 200) {
      // then parse the JSON.
      // print("response.body:" + response.body);

      // decode the data
      var responseJson = jsonDecode(response.body);
      return (responseJson["signal"] as List)
          .map((data) => TrendingSignalModel.fromJson(data))
          .toList();
    } else {
      return null;
    }
  }

  // Get trending signal data by ID
  Future<TrendingSignalModel> getTrendingByID({int id}) async {
    // Get user token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    // Merge url into endpoint
    Uri url = Uri.parse("$fullBaseURI/api/signal/$id");

    // get data
    var response = await http.get(url, headers: <String, String>{
      'Content-Type': 'application/json',
      'Charset': 'utf-8',
      'Authorization': 'Bearer $token'
    });
    print(response.statusCode.toString());

    if (response.statusCode == 200) {
      // then parse the JSON.
      // print("response.body:" + response.body);

      // decode the data
      var responseJson = jsonDecode(response.body);
      return TrendingSignalModel.fromJson(responseJson["signalDetail"]);
    } else {
      // then throw an exception.
      return null;
    }
  }

  // Post Comment on the signal
  Future<int> doSignalReaction({int signalID, String reaction}) async {
    // Get user token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    // post and get response
    final response = await http.post(
      Uri.https(baseURI, '/api/react'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Charset': 'utf-8',
        'Authorization': 'Bearer $token'
      },
      body: jsonEncode(
          <String, dynamic>{'signalId': signalID, 'reaction': reaction}),
    );

    print(response.statusCode);
    print("response.body:" + response.body);

    return response.statusCode;
  }
}
