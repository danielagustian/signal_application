import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signal_app/models/comment-model.dart';

class CommentService {
  final baseURI = "api.traderindo.com";
  final fullBaseURI = "https://api.traderindo.com";

  // Get all comment in signal
  Future<List<CommentModel>> getAllComment({int signalID}) async {
    // Get user token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    // Merge url into endpoint
    Uri url = Uri.parse("$fullBaseURI/api/comment/$signalID");

    // Get data
    var response = await http.get(url, headers: <String, String>{
      'Content-Type': 'application/json',
      'Charset': 'utf-8',
      'Authorization': 'Bearer $token'
    });

    print("Comment");
    print(response.statusCode.toString());

    if (response.statusCode == 200) {
      // then parse the JSON.
      // print("response.body:" + response.body);

      // decode the data
      var responseJson = jsonDecode(response.body);
      return (responseJson["comment"] as List)
          .map((data) => CommentModel.fromJson(data))
          .toList();
    } else {
      return null;
    }
  }

  // Post Comment on the signal
  Future<int> doCommentSignal({int signalID, String comment}) async {
    // Get user token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    // post and get response
    final response = await http.post(
      Uri.https(baseURI, '/api/comment'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Charset': 'utf-8',
        'Authorization': 'Bearer $token'
      },
      body: jsonEncode(
          <String, dynamic>{'signalId': signalID, 'comment': comment}),
    );

    print(response.statusCode);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      // then parse the JSON.
      // print("response.body:" + response.body);

      return 200;
    } else {
      // then throw an exception.
      return response.statusCode;
    }
  }

  // Reply Comment on the signal
  Future<int> doReplySignal({int commentID, String comment}) async {
    // Get user token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    // post and get response
    final response = await http.post(
      Uri.https(baseURI, '/api/comment/child-comment'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Charset': 'utf-8',
        'Authorization': 'Bearer $token'
      },
      body: jsonEncode(
          <String, dynamic>{'commentId': commentID, 'comment': comment}),
    );

    print(response.statusCode);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      // then parse the JSON.
      // print("response.body:" + response.body);

      return 200;
    } else {
      // then throw an exception.
      return response.statusCode;
    }
  }

  // Post Comment on the signal
  Future<int> doVote({int commentID, String vote, bool child}) async {
    // Get user token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    // post and get response
    final response = child
        ? await http.post(
            Uri.https(baseURI, '/api/comment/child-comment/like'),
            headers: <String, String>{
              'Content-Type': 'application/json',
              'Charset': 'utf-8',
              'Authorization': 'Bearer $token'
            },
            body: jsonEncode(<String, dynamic>{
              'childCommentId': commentID,
              'reaction': vote
            }),
          )
        : await http.post(
            Uri.https(baseURI, '/api/comment/like'),
            headers: <String, String>{
              'Content-Type': 'application/json',
              'Charset': 'utf-8',
              'Authorization': 'Bearer $token'
            },
            body: jsonEncode(
                <String, dynamic>{'commentId': commentID, 'reaction': vote}),
          );

    print(response.statusCode);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      // then parse the JSON.
      // print("response.body:" + response.body);

      return 200;
    } else {
      // then throw an exception.
      return response.statusCode;
    }
  }
}
