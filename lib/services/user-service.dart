import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signal_app/models/comment-model.dart';
import 'package:signal_app/models/user-model.dart';

class UserService {
  final baseURI = "api.traderindo.com";
  final fullBaseURI = "https://api.traderindo.com";

  // Get all data
  Future<List<UserModel>> getAllUserData() async {
    // Get user token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    // Merge url into endpoint
    Uri url = Uri.parse("$fullBaseURI/api/user");

    // Get data
    var response = await http.get(url, headers: <String, String>{
      'Content-Type': 'application/json',
      'Charset': 'utf-8',
      'Authorization': 'Bearer $token'
    });

    print(response.statusCode.toString());

    if (response.statusCode == 200) {
      // then parse the JSON.
      // print("response.body:" + response.body);

      // decode the data
      var responseJson = jsonDecode(response.body);
      return (responseJson["user"] as List)
          .map((data) => UserModel.fromJson(data))
          .toList();
    } else {
      return null;
    }
  }

  // Get user profile data by ID
  Future<UserModel> getUserProfile({int uID}) async {
    // Get user token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    // Merge url into endpoint
    Uri url = Uri.parse("$fullBaseURI/api/profile/$uID");

    // Get data
    var response = await http.get(url, headers: <String, String>{
      'Content-Type': 'application/json',
      'Charset': 'utf-8',
      'Authorization': 'Bearer $token'
    });

    print(response.statusCode.toString());

    if (response.statusCode == 200) {
      // then parse the JSON.
      // print("response.body:" + response.body);

      // decode the data
      var responseJson = jsonDecode(response.body);
      return UserModel.fromJson(responseJson["user"]);
    } else {
      return null;
    }
  }

  // Handle Follow and Unfollow  user
  Future doFollowOrUnfollow({int userID}) async {
    final response = await http.post(
      Uri.https(baseURI, '/api/follow'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Charset': 'utf-8'
      },
      body: jsonEncode(<String, dynamic>{'user_id': userID}),
    );
    print(response.statusCode.toString());

    String msg = "";
    if (response.statusCode == 200) {
      // If the server did return a 200's CREATED response,
      // then parse the JSON.
      print("response.body:" + response.body);
      var jsonResult = jsonDecode(response.body);

      msg = jsonResult["message"];
      // final user = AuthModel.fromJson(jsonDecode(response.body));

      return msg;
    }
    // If the server did not return a 200's response,
    // then throw an exception.
    // throw Exception('Failed to login');
    return msg;
  }
}
