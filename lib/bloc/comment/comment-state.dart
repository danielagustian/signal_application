import 'package:equatable/equatable.dart';
import 'package:signal_app/models/comment-model.dart';

abstract class CommentState extends Equatable {
  const CommentState();

  List<Object> get props => [];
}

class CommentStateLoading extends CommentState {}

class CommentStateFailLoad extends CommentState {}

class CommentStateSuccessLoad extends CommentState {
  final List<CommentModel> comments;

  CommentStateSuccessLoad(this.comments);

  List<Object> get props => [comments];
}
