import 'package:equatable/equatable.dart';
import 'package:signal_app/models/comment-model.dart';

abstract class CommentEvent extends Equatable {
  const CommentEvent();

  List<Object> get props => [];
}

// Event to post comment in the signal by ID
class CommentEventPostComment extends CommentEvent {
  final int signalID;
  final String comment;
  const CommentEventPostComment({this.signalID, this.comment});

  @override
  List<Object> get props => [signalID, comment];
}

// Event to post a child comment in parent comment by commentID
class CommentEventReplyComment extends CommentEvent {
  final int commentID, signalID;
  final String comment;
  const CommentEventReplyComment({this.commentID, this.comment, this.signalID});

  @override
  List<Object> get props => [commentID, comment, signalID];
}

// Event to post a child comment in parent comment by commentID
class CommentEventVote extends CommentEvent {
  final int commentID, signalID;
  final String reaction;
  final bool child;
  const CommentEventVote({
    this.commentID,
    this.reaction,
    this.signalID,
    this.child: false,
  });

  @override
  List<Object> get props => [commentID, reaction, signalID, child];
}

// Event to load all comment from the signal
class CommentEventLoadCommentSignal extends CommentEvent {
  final int signalID;
  const CommentEventLoadCommentSignal({this.signalID});

  @override
  List<Object> get props => [signalID];
}

class CommentEventUpdated extends CommentEvent {
  final List<CommentModel> comments;
  const CommentEventUpdated(this.comments);

  @override
  List<Object> get props => [comments];
}
