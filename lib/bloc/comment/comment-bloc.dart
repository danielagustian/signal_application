import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:signal_app/bloc/comment/comment-event.dart';
import 'package:signal_app/bloc/comment/comment-state.dart';
import 'package:signal_app/services/comment-service.dart';

class CommentBloc extends Bloc<CommentEvent, CommentState> {
  StreamSubscription _subscription;
  final CommentService _commentService = CommentService();

  CommentBloc() : super(null);

  // BLOC for fetch all comment in the signal
  Stream<CommentState> mapAllSignalComment(
      CommentEventLoadCommentSignal event) async* {
    yield CommentStateLoading();
    // return list of comment
    var res = await _commentService.getAllComment(signalID: event.signalID);

    // ascending sorting by like
    res.sort((x, y) => y.like.compareTo(x.like));

    // directly throw into success load or fail load
    if (res.length > 0 && res != null) {
      yield CommentStateSuccessLoad(res);
    } else {
      yield CommentStateFailLoad();
    }
  }

  // BLOC for post comment on signal
  Stream<CommentState> mapPostingComment(CommentEventPostComment event) async* {
    yield CommentStateLoading();
    // return auth model
    var res = await _commentService.doCommentSignal(
        comment: event.comment, signalID: event.signalID);

    // directly throw into success load or fail load
    if (res >= 200 && res < 300) {
      add(CommentEventLoadCommentSignal(signalID: event.signalID));
    } else {
      yield CommentStateFailLoad();
    }
  }

  // BLOC for reply comment on signal
  Stream<CommentState> mapReplyComment(CommentEventReplyComment event) async* {
    yield CommentStateLoading();
    // return auth model
    var res = await _commentService.doReplySignal(
        comment: event.comment, commentID: event.commentID);

    // directly throw into success load or fail load
    if (res >= 200 && res < 300) {
      add(CommentEventLoadCommentSignal(signalID: event.signalID));
    } else {
      yield CommentStateFailLoad();
    }
  }

  // BLOC for vote the comment on signal
  Stream<CommentState> mapVoteComment(CommentEventVote event) async* {
    yield CommentStateLoading();
    // return auth model
    var res = await _commentService.doVote(
        vote: event.reaction, commentID: event.commentID, child: event.child);

    // directly throw into success load or fail load
    if (res >= 200 && res < 300) {
      add(CommentEventLoadCommentSignal(signalID: event.signalID));
    } else {
      yield CommentStateFailLoad();
    }
  }

  // BLOC for update the state when the user doing event
  Stream<CommentState> mapUpdatingCommentState(
      CommentEventUpdated event) async* {
    yield CommentStateSuccessLoad(event.comments);
  }

  @override
  Stream<CommentState> mapEventToState(CommentEvent event) async* {
    if (event is CommentEventLoadCommentSignal) {
      yield* mapAllSignalComment(event);
    } else if (event is CommentEventPostComment) {
      yield* mapPostingComment(event);
    } else if (event is CommentEventReplyComment) {
      yield* mapReplyComment(event);
    } else if (event is CommentEventVote) {
      yield* mapVoteComment(event);
    } else if (event is CommentEventUpdated) {
      yield* mapUpdatingCommentState(event);
    }
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }
}
