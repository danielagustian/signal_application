import 'package:equatable/equatable.dart';
import 'package:signal_app/models/trending-signal-model.dart';

abstract class TrendingState extends Equatable {
  const TrendingState();

  List<Object> get props => [];
}

class TrendingStateLoading extends TrendingState {}

class TrendingStateFailLoad extends TrendingState {}

// Load detail signal
class TrendingStateLoadDetail extends TrendingState {
  final TrendingSignalModel trend;

  TrendingStateLoadDetail({this.trend});

  List<Object> get props => [trend];
}

// Load all trending signal data
class TrendingStateSuccessLoad extends TrendingState {
  final List<TrendingSignalModel> trendings; 
  TrendingStateSuccessLoad({this.trendings});

  List<Object> get props => [trendings];

  @override
  String toString() {
    return 'Data : { Trending List: $trendings }';
  }
}

// Load all filtered trending signal data
class TrendingStateSuccessLoadFilteredSignal extends TrendingState {
  final List<TrendingSignalModel> trendings; 
  TrendingStateSuccessLoadFilteredSignal({this.trendings });

  List<Object> get props => [trendings];

  @override
  String toString() {
    return 'Data : { Trending List: $trendings }';
  }
}
