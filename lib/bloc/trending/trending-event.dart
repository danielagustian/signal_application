import 'package:equatable/equatable.dart';
import 'package:signal_app/models/trending-signal-model.dart';

abstract class TrendingEvent extends Equatable {
  const TrendingEvent();

  List<Object> get props => [];
}

// Event to load all trending data
class TrendingEventLoadDetailData extends TrendingEvent {
  final int id;
  TrendingEventLoadDetailData({this.id});

  @override
  List<Object> get props => [id];
}

// Event to load all trending data
class TrendingEventLoadData extends TrendingEvent {
  TrendingEventLoadData();

  @override
  List<Object> get props => [];
}

// Event to load all filtered trending data
class TrendingEventLoadFilteredData extends TrendingEvent {
  final String queryParams;
  TrendingEventLoadFilteredData({this.queryParams});

  @override
  List<Object> get props => [queryParams];
}

// Event to handle login using form
class TrendingEventUpdated extends TrendingEvent {
  final List<TrendingSignalModel> trendings;

  TrendingEventUpdated({this.trendings});

  @override
  List<Object> get props => [trendings];
}
