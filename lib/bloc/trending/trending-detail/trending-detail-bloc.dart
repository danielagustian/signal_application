import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:signal_app/bloc/trending/trending-detail/trending-detail-event.dart';
import 'package:signal_app/bloc/trending/trending-detail/trending-detail-state.dart';
import 'package:signal_app/services/trending-service.dart';

class TrendingDetailBloc
    extends Bloc<TrendingDetailEvent, TrendingDetailState> {
  StreamSubscription _subscription;
  final TrendingService _trendingService = TrendingService();

  TrendingDetailBloc() : super(null);

  // BLOC for fetch detail signal
  Stream<TrendingDetailState> mapFetchDetailSignal(
      TrendingDetailEventLoadSignal event) async* {
    yield TrendingDetailStateLoading();
    // return trending signal model
    var res = await _trendingService.getTrendingByID(id: event.signalID);

    // directly throw into success load or fail load
    if (res != null) {
      yield TrendingDetailStateSuccessLoad(res);
    } else {
      yield TrendingDetailStateFailLoad();
    }
  }

  // BLOC for add reaction in the signal
  Stream<TrendingDetailState> mapAddSignalReaction(
      TrendingDetailEventAddReaction event) async* {
    yield TrendingDetailStateLoading();
    // return status code
    var res = await _trendingService.doSignalReaction(
        reaction: event.reaction, signalID: event.signalID);

    // directly throw into success load or fail load
    if (res >= 200 && res < 300) {
      add(TrendingDetailEventLoadSignal(signalID: event.signalID));
      // yield TrendingDetailStateSuccessReact(reaction: event.reaction);
    } else {
      yield TrendingDetailStateFailLoad();
    }
  }

  // BLOC for update the state when the user doing event
  Stream<TrendingDetailState> mapUpdatingTrendingDetailState(
      TrendingDetailSignalEventUpdated event) async* {
    yield TrendingDetailStateSuccessLoad(event.trend);
  }

  @override
  Stream<TrendingDetailState> mapEventToState(
      TrendingDetailEvent event) async* {
    if (event is TrendingDetailEventLoadSignal) {
      yield* mapFetchDetailSignal(event);
    } else if (event is TrendingDetailEventAddReaction) {
      yield* mapAddSignalReaction(event);
    } else if (event is TrendingDetailSignalEventUpdated) {
      yield* mapUpdatingTrendingDetailState(event);
    }
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }
}
