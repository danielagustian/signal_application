import 'package:equatable/equatable.dart';
import 'package:signal_app/models/trending-signal-model.dart';

abstract class TrendingDetailEvent extends Equatable {
  const TrendingDetailEvent();

  List<Object> get props => [];
}

// Event to handle reaction in the signal by ID
class TrendingDetailEventAddReaction extends TrendingDetailEvent {
  final int signalID;
  final String reaction;
  const TrendingDetailEventAddReaction({this.signalID, this.reaction});

  @override
  List<Object> get props => [signalID, reaction];
}

// Event to the detail signal
class TrendingDetailEventLoadSignal extends TrendingDetailEvent {
  final int signalID;
  const TrendingDetailEventLoadSignal({this.signalID});

  @override
  List<Object> get props => [signalID];
}

class TrendingDetailSignalEventUpdated extends TrendingDetailEvent {
  final TrendingSignalModel trend;
  const TrendingDetailSignalEventUpdated(this.trend);

  @override
  List<Object> get props => [trend];
}
