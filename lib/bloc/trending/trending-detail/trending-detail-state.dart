import 'package:equatable/equatable.dart';
import 'package:signal_app/models/trending-signal-model.dart';

abstract class TrendingDetailState extends Equatable {
  const TrendingDetailState();

  List<Object> get props => [];
}

class TrendingDetailStateLoading extends TrendingDetailState {}

class TrendingDetailStateFailLoad extends TrendingDetailState {}

class TrendingDetailStateSuccessReact extends TrendingDetailState {
  final String reaction;

  TrendingDetailStateSuccessReact({this.reaction});

  List<Object> get props => [reaction];
}

class TrendingDetailStateFailReact extends TrendingDetailState {
  final String reaction;

  TrendingDetailStateFailReact({this.reaction});

  List<Object> get props => [reaction];
}

class TrendingDetailStateSuccessLoad extends TrendingDetailState {
  final TrendingSignalModel signal;

  TrendingDetailStateSuccessLoad(this.signal);

  List<Object> get props => [signal];
}
