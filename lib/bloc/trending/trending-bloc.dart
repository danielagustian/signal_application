import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:signal_app/bloc/trending/trending-event.dart';
import 'package:signal_app/bloc/trending/trending-state.dart';
import 'package:signal_app/services/trending-service.dart';

class TrendingBloc extends Bloc<TrendingEvent, TrendingState> {
  StreamSubscription _subscription;
  final TrendingService _trendingService = TrendingService();

  TrendingBloc() : super(null);

// BLOC for map all trending data
  Stream<TrendingState> mapAllTrendingData() async* {
    yield TrendingStateLoading();

    // return list / independent model
    var res = await _trendingService.getAllData();
    // directly throw into success load or fail load
    if (res != null) {
      yield TrendingStateSuccessLoad(trendings: res);
    } else {
      yield TrendingStateFailLoad();
    }
  }

  // BLOC for map filtered trending data
  Stream<TrendingState> mapFilteredTrendingData(
      TrendingEventLoadFilteredData event) async* {
    yield TrendingStateLoading();

    // return list / independent model
    var res;

    // check is there is an query to filter or not
    if (event.queryParams == null || event.queryParams == "") {
      print("masuk");
      res = await _trendingService.getAllData();
    } else {
      res = await _trendingService.getAllData(query: event.queryParams);
    }
    // directly throw into success load or fail load
    if (res != null) {
      yield TrendingStateSuccessLoadFilteredSignal(trendings: res);
    } else {
      yield TrendingStateFailLoad();
    }
  }

  // BLOC for map all trending data
  Stream<TrendingState> mapTrendingDataUsingID(
      TrendingEventLoadDetailData event) async* {
    yield TrendingStateLoading();
    // return list / independent model
    var res = await _trendingService.getTrendingByID(id: event.id);

    // directly throw into success load or fail load
    if (res != null) {
      yield TrendingStateLoadDetail(trend: res);
    } else {
      yield TrendingStateFailLoad();
    }
  }

  // BLOC for update the state when the user doing event
  Stream<TrendingState> mapUpdatingTrendingState(
      TrendingEventUpdated event) async* {
    yield TrendingStateSuccessLoad(trendings: event.trendings);
  }

  @override
  Stream<TrendingState> mapEventToState(TrendingEvent event) async* {
    if (event is TrendingEventLoadData) {
      yield* mapAllTrendingData();
    } else if (event is TrendingEventLoadFilteredData) {
      yield* mapFilteredTrendingData(event);
    } else if (event is TrendingEventLoadDetailData) {
      yield* mapTrendingDataUsingID(event);
    } else if (event is TrendingEventUpdated) {
      yield* mapUpdatingTrendingState(event);
    }
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }
}
