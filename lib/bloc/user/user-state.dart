import 'package:equatable/equatable.dart';
import 'package:signal_app/models/user-model.dart';

abstract class UserState extends Equatable {
  const UserState();

  List<Object> get props => [];
}

class UserStateLoading extends UserState {}

class UserStateFailLoad extends UserState {}

class UserStateFailFollowORUnfollow extends UserState {}

class UserStateSuccessLoad extends UserState {
  final List<UserModel> users;

  UserStateSuccessLoad({this.users});

  List<Object> get props => [users];
}

class UserProfileStateSuccessLoad extends UserState {
  final UserModel user;

  UserProfileStateSuccessLoad(this.user);

  List<Object> get props => [user];
}
