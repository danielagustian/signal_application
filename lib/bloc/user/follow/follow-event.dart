import 'dart:collection';

import 'package:equatable/equatable.dart';
import 'package:signal_app/models/comment-model.dart';
import 'package:signal_app/models/user-model.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();

  List<Object> get props => [];
}

// Event to load all user
class UserEventLoadData extends UserEvent {
  const UserEventLoadData();

  @override
  List<Object> get props => [];
}

// Event to load user profile
class UserProfileEventLoadData extends UserEvent {
  final int userID;
  const UserProfileEventLoadData({this.userID});

  @override
  List<Object> get props => [userID];
}

class UserEventUpdated extends UserEvent {
  final List<UserModel> users;
  const UserEventUpdated(this.users);

  @override
  List<Object> get props => [users];
}

class UserProfileEventUpdated extends UserEvent {
  final UserModel user;
  const UserProfileEventUpdated(this.user);

  @override
  List<Object> get props => [user];
}
