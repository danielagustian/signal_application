import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:signal_app/bloc/user/user-event.dart';
import 'package:signal_app/bloc/user/user-state.dart';
import 'package:signal_app/services/user-service.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  StreamSubscription _subscription;
  final UserService _userService = UserService();

  UserBloc() : super(null);

  // BLOC for fetch all comment in the signal
  Stream<UserState> mapAllUserData() async* {
    yield UserStateLoading();
    // return list of user 
    var res = await _userService.getAllUserData();

    // directly throw into success load or fail load
    if (res != null && res.length > 0) {
      yield UserStateSuccessLoad(users: res);
    } else {
      yield UserStateFailLoad();
    }
  }

  // BLOC for fetch all comment in the signal
  Stream<UserState> mapUserProfile(UserProfileEventLoadData event) async* {
    yield UserStateLoading();
    // return user model
    var res = await _userService.getUserProfile(uID: event.userID);

    // directly throw into success load or fail load
    if (res != null) {
      yield UserProfileStateSuccessLoad(res);
    } else {
      yield UserStateFailLoad();
    }
  }

  // BLOC for update the state when the user doing event
  Stream<UserState> mapUpdatingUserState(UserEventUpdated event) async* {
    yield UserStateSuccessLoad(users: event.users);
  }

  // BLOC for update the state when the user doing event
  Stream<UserState> mapUpdatingUserProfileState(
      UserProfileEventUpdated event) async* {
    yield UserProfileStateSuccessLoad(event.user);
  }

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    if (event is UserEventLoadData) {
      yield* mapAllUserData();
    } else if (event is UserProfileEventLoadData) {
      yield* mapUserProfile(event);
    } else if (event is UserEventUpdated) {
      yield* mapUpdatingUserState(event);
    }
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }
}
