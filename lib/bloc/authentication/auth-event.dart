import 'package:equatable/equatable.dart';
import 'package:signal_app/models/auth-model.dart';
import 'package:signal_app/models/user-model.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  List<Object> get props => [];
}

// Event to handle login using form
class AuthEventLoginForm extends AuthenticationEvent {
  final String email, password;
  final bool show;
  const AuthEventLoginForm({this.email, this.password, this.show});

  @override
  List<Object> get props => [email, password, show];
}

// Event to handle register using form
class AuthEventRegisterForm extends AuthenticationEvent {
  final UserModel user;
  final bool show;

  const AuthEventRegisterForm({this.user, this.show});

  @override
  List<Object> get props => [user, show];
}

class AuthEventWithGoogle extends AuthenticationEvent {
  const AuthEventWithGoogle();

  @override
  List<Object> get props => [];
}

class AuthEventUpdated extends AuthenticationEvent {
  final AuthModel user;
  const AuthEventUpdated(this.user);

  @override
  List<Object> get props => [user];
}
