import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signal_app/bloc/authentication/auth-event.dart';
import 'package:signal_app/bloc/authentication/auth-state.dart';
import 'package:signal_app/models/auth-model.dart';
import 'package:signal_app/services/authentication-service.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  StreamSubscription _subscription;
  final AuthenticationService _authenticationService = AuthenticationService();

  AuthenticationBloc() : super(null);

  // BLOC for login with google
  Stream<AuthenticationState> mapLoginGoogle() async* {
    // return user model
    // var res = await _authenticationRepository.signInWithGoogle();

    // // directly throw into success load or fail load
    // if (res is AuthModel && res != null) {
    //   yield AuthStateSuccessLoad(res);
    // } else {
    //   yield AuthStateFailLoad();
    // }
  }

  // BLOC for login manually using email and password
  Stream<AuthenticationState> mapFormLogin(AuthEventLoginForm event) async* {
    yield AuthStateLoading();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // return auth model
    var res = await _authenticationService.login(
        email: event.email, password: event.password);

    // directly throw into success load or fail load
    if (res is AuthModel && res != null) {
      // Save is user want to keep the session or not
      await prefs.setBool("remember", event.show);

      // Save token and id user
      await prefs.setString("token", res.token);
      await prefs.setInt("userID", res.user.userID);
      yield AuthStateSuccessLoad(res);
    } else {
      yield AuthStateFailLoad();
    }
  }

  // BLOC for login manually using email and password
  Stream<AuthenticationState> mapFormLRegister(
      AuthEventRegisterForm event) async* {
    yield AuthStateLoading();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // return auth model
    var res = await _authenticationService.register(user: event.user);

    // directly throw into success load or fail load
    if (res is AuthModel) {
      // Save is user want to keep the session or not
      await prefs.setBool("remember", event.show);

      // Save token and id user
      await prefs.setString("token", res.token);
      await prefs.setInt("userID", res.user.userID);
      yield AuthStateSuccessLoad(res);
    } else {
      if (res == "") {
        yield AuthStateFailLoad();
      } else {
        yield res == "email" ? AuthStateFailEmail() : AuthStateFailUsername();
      }
    }
  }

  // BLOC for update the state when the user doing event
  Stream<AuthenticationState> mapUpdatingAuthState(
      AuthEventUpdated event) async* {
    yield AuthStateSuccessLoad(event.user);
  }

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is AuthEventLoginForm) {
      yield* mapFormLogin(event);
    } else if (event is AuthEventRegisterForm) {
      yield* mapFormLRegister(event);
    } else if (event is AuthEventUpdated) {
      yield* mapUpdatingAuthState(event);
    }
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }
}
